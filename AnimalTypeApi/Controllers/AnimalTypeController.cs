﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AnimalTypeAPI.Data;
using AnimalTypeAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AnimalTypeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalTypeController : Controller
    {
        private readonly IAnimalTypeDbContext _animalTypeDbContext;

        public AnimalTypeController(IAnimalTypeDbContext animalTypeDbContext)
        {
            if (animalTypeDbContext == null)
            {
                throw new InvalidOperationException();
            }
            _animalTypeDbContext = animalTypeDbContext;
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody]AnimalType animalType)
        {
            if (animalType == null ||
                animalType.Id == Guid.Empty)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

            try
            {
                var existingAnimalType = await _animalTypeDbContext.AnimalTypes.FindAsync(animalType.Id);
                if (existingAnimalType != null)
                {
                    return Conflict();
                }
                _animalTypeDbContext.AnimalTypes.Add(animalType);
                await _animalTypeDbContext.SaveChangesAsync();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            return Json(animalType);
        }

        [HttpGet()]
        public ActionResult Get()
        {
            IEnumerable<AnimalType> animalType = _animalTypeDbContext.AnimalTypes;
            if (animalType == null)
            {
                return NotFound();
            }

            return Json(animalType);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            if (id == Guid.Empty )
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

            AnimalType animalType = await _animalTypeDbContext.AnimalTypes.FindAsync(id);
            if (animalType == null)
            {
                return NotFound();
            }

            return Json(animalType);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(AnimalType animalType)
        {
            if (animalType == null)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

            try
            {
                var existingAnimalType = await _animalTypeDbContext.AnimalTypes.FindAsync(animalType.Id);
                if (existingAnimalType == null)
                {
                    return NotFound();
                }

                existingAnimalType.FoodSatisfactionGain = animalType.FoodSatisfactionGain;
                existingAnimalType.FoodSatisfactionLoss = animalType.FoodSatisfactionLoss;
                existingAnimalType.HappynessGain = animalType.HappynessGain;
                existingAnimalType.HappynessLoss = animalType.HappynessLoss;
                existingAnimalType.Name = animalType.Name;
                _animalTypeDbContext.AnimalTypes.Update(existingAnimalType);

                await _animalTypeDbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            return Json(animalType);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (!id.HasValue ||
                id.Value == Guid.Empty)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }
            try
            {
                var animal = await _animalTypeDbContext.AnimalTypes.FindAsync(id);
                if (animal == null)
                {
                    return NotFound();
                }

                _animalTypeDbContext.AnimalTypes.Remove(animal);
                await _animalTypeDbContext.SaveChangesAsync();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
            return StatusCode((int)HttpStatusCode.OK);
        }
    }
}