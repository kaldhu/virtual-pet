﻿using System;

namespace AnimalTypeAPI.Models
{
    public class AnimalType
    {
        public virtual Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; } = "Unknown";

        private static readonly int _defaultHappynessGain = 1;
        public int HappynessGain { get; set; } = _defaultHappynessGain;


        private static readonly int _defaultHappynessLoss = 1;
        public int HappynessLoss { get; set; } = _defaultHappynessLoss;


        private static readonly int _defaultFoodSatisfactionGain = 1;
        public int FoodSatisfactionGain { get; set; } = _defaultFoodSatisfactionGain;


        private static readonly int _defaultFoodSatisfactionLoss = 1;
        public int FoodSatisfactionLoss { get; set; } = _defaultFoodSatisfactionLoss;
    }
}
