﻿using AnimalTypeAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace AnimalTypeAPI.Data
{
    public interface IAnimalTypeDbContext
    {
        #region DBContext Methods
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        #endregion
        DbSet<AnimalType> AnimalTypes { get;set;}
    }
}
