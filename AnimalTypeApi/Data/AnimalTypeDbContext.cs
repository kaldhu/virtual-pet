﻿using AnimalTypeAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AnimalTypeAPI.Data
{
    public class AnimalTypeDbContext : DbContext, IAnimalTypeDbContext
    {
        public AnimalTypeDbContext(DbContextOptions<AnimalTypeDbContext> options)
              : base(options)
        {
        }

        public DbSet<AnimalType> AnimalTypes { get; set; }
    }
}
