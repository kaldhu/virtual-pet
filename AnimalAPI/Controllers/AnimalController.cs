﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AnimalAPI.Data;
using AnimalAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace AnimalAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalController : Controller
    {
        private readonly IAnimalDbContext _animalDbContext;

        public AnimalController(IAnimalDbContext animalDbContext)
        {
            if (animalDbContext == null)
            {
                throw new InvalidOperationException();
            }
            _animalDbContext = animalDbContext;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody]Animal animal, Guid? userId)
        {
            if (!userId.HasValue ||
                userId.Value == Guid.Empty ||
                animal == null ||
                animal.Id == Guid.Empty ||
                animal.AnimalTypeId == Guid.Empty ||
                animal.UserId == Guid.Empty)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

            if (animal.UserId != userId.Value)
            {
                return Unauthorized();
            }

            try
            {
                var existingAnimalType = await _animalDbContext.Animals.FindAsync(animal.Id);
                if (existingAnimalType != null)
                {
                    return Conflict();
                }
                _animalDbContext.Animals.Add(animal);
                await _animalDbContext.SaveChangesAsync();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            return Json(animal);
        }

        [HttpGet()]
        public ActionResult Get()
        {
            IEnumerable<Animal> animals = _animalDbContext.Animals;
            if (animals == null)
            {
                return NotFound();
            }

            return Json(animals);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id, Guid? userId)
        {
            if(id == Guid.Empty || userId == Guid.Empty)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

            Animal animal = await _animalDbContext.Animals.FindAsync(id);
            if (animal == null)
            {
                return NotFound();
            }

            if(animal.UserId != userId)
            {
                return Unauthorized();
            }

            return Json(animal);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(Animal animal, Guid? userId)
        {
            if (!userId.HasValue ||
                userId.Value == Guid.Empty ||
                animal == null)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

            if(userId != animal.UserId)
            {
                return Unauthorized();
            }

            try
            {
                var existingAnimal = await _animalDbContext.Animals.FindAsync(animal.Id);
                if (existingAnimal == null)
                {
                    return NotFound();
                }
                existingAnimal.LastUpdated = DateTime.UtcNow;
                existingAnimal.AnimalTypeId = animal.AnimalTypeId;
                existingAnimal.FoodSatisfaction = animal.FoodSatisfaction;
                existingAnimal.Happyness = animal.Happyness;
                existingAnimal.UserId = animal.UserId;

                _animalDbContext.Animals.Update(existingAnimal);
                await _animalDbContext.SaveChangesAsync();
                return Json(existingAnimal);
            } 
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid? id, Guid? userId)
        {
            if (!userId.HasValue ||
                userId.Value == Guid.Empty ||
                !id.HasValue ||
                id.Value == Guid.Empty)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }
            try
            {
                var animal = await _animalDbContext.Animals.FindAsync(id);
                if(animal == null)
                {
                    return NotFound();
                }

                if(animal.UserId != userId.Value)
                {
                    return Unauthorized();
                }
                _animalDbContext.Animals.Remove(animal);
                await _animalDbContext.SaveChangesAsync();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
            return StatusCode((int)HttpStatusCode.OK);
        }
    }
}