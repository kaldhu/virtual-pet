﻿using AnimalAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace AnimalAPI.Data
{
    public interface IAnimalDbContext
    {
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        DbSet<Animal> Animals { get;set;}
    }
}
