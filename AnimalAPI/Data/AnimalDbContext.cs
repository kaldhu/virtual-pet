﻿using AnimalAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AnimalAPI.Data
{
    public class AnimalDbContext : DbContext, IAnimalDbContext
    {
        public AnimalDbContext(DbContextOptions<AnimalDbContext> options)
              : base(options)
        {
        }

        public DbSet<Animal> Animals { get; set; }
    }
}
