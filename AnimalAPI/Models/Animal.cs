﻿using System;

namespace AnimalAPI.Models
{
    public class Animal
    {
        public virtual Guid Id { get; set; } = Guid.NewGuid();

        //TODO: Add HATEOS link to AnimalType within Json Export
        public virtual Guid AnimalTypeId { get; set; } = Guid.Empty;

        public virtual Guid UserId { get; set; } = Guid.Empty;

        private static readonly int _defaultHappyness = 0;
        public int Happyness { get; set; } = _defaultHappyness;

        private static readonly int _defaultFoodSatisfaction = 0;
        public int FoodSatisfaction { get; set; } = _defaultFoodSatisfaction;

        public DateTime LastUpdated { get; set; } = DateTime.Now;
    }
}
