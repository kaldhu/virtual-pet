﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPetAPI.Models
{
    public class Config
    {
        public string AnimalProcessingAPIAddress { get; set; }
        public string AnimalAPIAddress { get; set; }
        public string AnimalTypeAPIAddress { get; set; }
    }
}
