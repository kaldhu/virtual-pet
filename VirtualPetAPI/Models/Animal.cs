﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualPetAPI.Models
{
    public class Animal
    {
        public virtual Guid UserID { get; set; }
        public virtual Guid AnimalTypeId { get; set; }
    }
}
