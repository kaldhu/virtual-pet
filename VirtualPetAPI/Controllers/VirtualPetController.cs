﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Services.Services;
using VirtualPetAPI.Models;

namespace VirtualPetAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VirtualPetController : Controller
    {
        private readonly IHttpWebRequestService _httpWebRequestService;
        private readonly IOptions<Config> _config;
        public VirtualPetController(IOptions<Config> config, IHttpWebRequestService httpWebRequestService)
        {
            if(config == null || httpWebRequestService == null)
            {
                throw new ArgumentNullException();
            }
            _config = config;
            _httpWebRequestService = httpWebRequestService;
        }

        [HttpPost]
        public async Task<ActionResult> Post(Guid animalTypeId, Guid userId)
        {
            var getAnimalTypeUriString = $"{_config.Value.AnimalTypeAPIAddress}?animalTypeId={animalTypeId}&userId={userId}";
            var getAnimalTypeWebRequest = _httpWebRequestService.BuildWebRequest(getAnimalTypeUriString, HttpMethods.Get);
            var getAnimalTypeResult = await _httpWebRequestService.GetWebResponse(getAnimalTypeWebRequest);

            if (getAnimalTypeResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)getAnimalTypeResult.StatusCode);
            }

            var postAnimalUriString = $"{_config.Value.AnimalAPIAddress}?userId={userId}";
            var postAnimalWebRequest = _httpWebRequestService.BuildWebRequest(postAnimalUriString, HttpMethods.Post);
            var animalData = new Animal()
            {
                AnimalTypeId = animalTypeId,
                UserID = userId
            };
            await _httpWebRequestService.AddDataToRequest(postAnimalWebRequest, animalData);
            var postResult = await _httpWebRequestService.GetWebResponse(postAnimalWebRequest);

            if (postResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)postResult.StatusCode);
            }
            return Content(postResult.Data, "application/json", Encoding.UTF8);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id, Guid userId)
        {
            var deleteAnimalUriString = $"{_config.Value.AnimalAPIAddress}/{id}?userId={userId}";
            var deleteAnimalWebRequest = _httpWebRequestService.BuildWebRequest(deleteAnimalUriString, HttpMethods.Delete);
            var deleteResult = await _httpWebRequestService.GetWebResponse(deleteAnimalWebRequest);

            if (deleteResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)deleteResult.StatusCode);
            }

            return Ok();
        }

        [HttpGet()]
        public async Task<ActionResult> Get(Guid userId)
        {
            var getAnimalUriString = $"{_config.Value.AnimalAPIAddress}?userId={userId}";
            var getAnimalWebRequest = _httpWebRequestService.BuildWebRequest(getAnimalUriString, HttpMethods.Get);
            var getResult = await _httpWebRequestService.GetWebResponse(getAnimalWebRequest);

            if (getResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)getResult.StatusCode);
            }

            return Content(getResult.Data, "application/json", Encoding.UTF8);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id, Guid userId)
        {
            var syncAnimalUriString = $"{_config.Value.AnimalProcessingAPIAddress}/{id}/sync?userId={userId}";
            var syncAnimalWebRequest = _httpWebRequestService.BuildWebRequest(syncAnimalUriString, HttpMethods.Put);
            var syncResult = await _httpWebRequestService.GetWebResponse(syncAnimalWebRequest);

            if (syncResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)syncResult.StatusCode);
            }

            var getAnimalUriString = $"{_config.Value.AnimalAPIAddress}/{id}?userId={userId}";
            var getAnimalWebRequest = _httpWebRequestService.BuildWebRequest(getAnimalUriString, HttpMethods.Get);
            var getResult = await _httpWebRequestService.GetWebResponse(getAnimalWebRequest);

            if (getResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)getResult.StatusCode);
            }

            return Content(getResult.Data, "application/json", Encoding.UTF8);
        }

        [HttpPut("{id}/feed")]
        public async Task<ActionResult> Feed(Guid id, Guid userId)
        {
            var syncAnimalUriString = $"{_config.Value.AnimalProcessingAPIAddress}/{id}/sync?userId={userId}";
            var syncAnimalWebRequest = _httpWebRequestService.BuildWebRequest(syncAnimalUriString, HttpMethods.Put);
            var syncResult = await _httpWebRequestService.GetWebResponse(syncAnimalWebRequest);

            if (syncResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)syncResult.StatusCode);
            }

            var feedAnimalUriString = $"{_config.Value.AnimalProcessingAPIAddress}/{id}/feed?userId={userId}";
            var feedAnimalWebRequest = _httpWebRequestService.BuildWebRequest(feedAnimalUriString, HttpMethods.Put);
            var feedResult = await _httpWebRequestService.GetWebResponse(feedAnimalWebRequest);

            if (feedResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)feedResult.StatusCode);
            }

            return Content(feedResult.Data, "application/json", Encoding.UTF8);
        }

        [HttpPut("{id}/stroke")]
        public async Task<ActionResult> Stroke(Guid id, Guid userId)
        {
            var syncAnimalUriString = $"{_config.Value.AnimalProcessingAPIAddress}/{id}/sync?userId={userId}";
            var syncAnimalWebRequest = _httpWebRequestService.BuildWebRequest(syncAnimalUriString, HttpMethods.Put);
            var syncResult = await _httpWebRequestService.GetWebResponse(syncAnimalWebRequest);

            if (syncResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)syncResult.StatusCode);
            }

            var strokeAnimalUriString = $"{_config.Value.AnimalProcessingAPIAddress}/{id}/stroke?userId={userId}";
            var strokeAnimalWebRequest = _httpWebRequestService.BuildWebRequest(strokeAnimalUriString, HttpMethods.Put);
            var strokeResult = await _httpWebRequestService.GetWebResponse(strokeAnimalWebRequest);

            if (strokeResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)strokeResult.StatusCode);
            }

            return Content(strokeResult.Data, "application/json", Encoding.UTF8);
        }
    }
}