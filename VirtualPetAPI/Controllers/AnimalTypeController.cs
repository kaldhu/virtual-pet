﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Services.Services;
using VirtualPetAPI.Models;

namespace VirtualPetAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalTypeController : Controller
    {
        private readonly IHttpWebRequestService _httpWebRequestService;
        private readonly IOptions<Config> _config;
        public AnimalTypeController(IOptions<Config> config, IHttpWebRequestService httpWebRequestService)
        {
            if (config == null || httpWebRequestService == null)
            {
                throw new ArgumentNullException();
            }
            _config = config;
            _httpWebRequestService = httpWebRequestService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            var getAnimalTypeUriString = $"{_config.Value.AnimalTypeAPIAddress}/{id}";
            var getAnimalTypeWebRequest = _httpWebRequestService.BuildWebRequest(getAnimalTypeUriString, HttpMethods.Get);
            var getResult = await _httpWebRequestService.GetWebResponse(getAnimalTypeWebRequest);

            if (getResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)getResult.StatusCode);
            }
            return Content(getResult.Data, "application/json", Encoding.UTF8);
        }


        [HttpGet()]
        public async Task<ActionResult> Get()
        {
            var getAnimalTypeUriString = $"{_config.Value.AnimalTypeAPIAddress}";
            var getAnimalTypeWebRequest = _httpWebRequestService.BuildWebRequest(getAnimalTypeUriString, HttpMethods.Get);
            var getResult = await _httpWebRequestService.GetWebResponse(getAnimalTypeWebRequest);

            if (getResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)getResult.StatusCode);
            }
            return Content(getResult.Data, "application/json", Encoding.UTF8);
        }
    }
}