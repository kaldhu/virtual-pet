﻿using AnimalAPI.Data;
using AnimalAPI.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Diagnostics.CodeAnalysis;

namespace NUnit_AnimalAPI.Data
{
    [ExcludeFromCodeCoverage]
    public class AnimalDbContextTests
    {
        AnimalDbContext _test_AnimalDbContext;
        [SetUp]
        public void Setup()
        {
            DbContextOptions<AnimalDbContext> dbContextOptions = new DbContextOptions<AnimalDbContext>();
            _test_AnimalDbContext = new AnimalDbContext(dbContextOptions);
        }

        [Test]
        public void Constructor_success()
        {
            AnimalDbContext test_AnimalDbContext;
            DbContextOptions<AnimalDbContext> test_dbContextOptions = new DbContextOptions<AnimalDbContext>();
            Assert.DoesNotThrow((() => test_AnimalDbContext = new AnimalDbContext(test_dbContextOptions)));
        }

        [Test]
        public void Inherits_DbContext()
        {
            Assert.IsInstanceOf(typeof(DbContext), _test_AnimalDbContext);
        }

        [Test]
        public void Contains_DBSet_Of_Animals()
        {
            Assert.IsInstanceOf(typeof(DbSet<Animal>), _test_AnimalDbContext.Animals);
        }
    }
}
