﻿using AnimalAPI.Controllers;
using AnimalAPI.Data;
using AnimalAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NUnit_AnimalAPI.Controllers
{
    [ExcludeFromCodeCoverage]
    public class AnimalControllerTests
    {
        private Mock<IAnimalDbContext> _moq_AnimalDbContext;
        private Mock<DbSet<Animal>> _moq_AnimalDbSet;
        private AnimalController _test_AnimalController;

        private Guid _animalId;
        private Guid _userId;
        private Guid _animalTypeId;
        private Guid _randomGuid;
        private Guid _emptyGuid;
        private Animal _animal;
        private Animal _putAnimal;
        private int _happyness;
        private int _foodSatisfaction;

        [SetUp]
        public void Setup()
        {
            _moq_AnimalDbContext = new Mock<IAnimalDbContext>();
            _moq_AnimalDbSet = new Mock<DbSet<Animal>>();

            _moq_AnimalDbContext.Setup(moq => moq.Animals).Returns(_moq_AnimalDbSet.Object);
            _test_AnimalController = new AnimalController(_moq_AnimalDbContext.Object);

            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            _emptyGuid = Guid.Empty;
            _userId = Guid.NewGuid();
            _randomGuid = Guid.NewGuid();
            _animalId = Guid.NewGuid();
            _animalTypeId = Guid.NewGuid();
            _happyness = 20;
            _foodSatisfaction = 10;

            _animal = new Animal()
            {
                //Id = _test_AnimalId,
                UserId = _userId,
                AnimalTypeId = _animalTypeId,
            };
            _putAnimal = new Animal()
            {
                //Id = _test_AnimalId,
                UserId = _userId,
                AnimalTypeId = _animalTypeId,
                FoodSatisfaction = _foodSatisfaction,
                Happyness = _happyness
            };
        }

        #region Constructor Tests

        [Test]
        public void Constructor_Success()
        {
            Assert.DoesNotThrow(() => new AnimalController(_moq_AnimalDbContext.Object));
        }

        [Test]
        public void Constructor_With_Invalid_DbContext_Param_Throws_Exception()
        {
            Assert.Throws<InvalidOperationException>(() => new AnimalController(null));
        }

        [Test]
        public void Constructor_Inherits_Controller()
        {
            Assert.IsInstanceOf<Controller>(_test_AnimalController);
        }

        #endregion

        #region Get Tests

        [Test]
        public async Task Get_With_Invalid_Id_Params_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Get(Guid.Empty, Guid.NewGuid());
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Get_With_Invalid_UserId_Params_Returns_BadRequest_StatusCode()
        {
           var result = await _test_AnimalController.Get(Guid.NewGuid(), Guid.Empty);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Get_Calls_FindAsync_For_Animals_From_DbContext()
        {
            await _test_AnimalController.Get(_randomGuid, _randomGuid);
            _moq_AnimalDbContext.Verify(moq => moq.Animals.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Get_Calls_FindAsync_For_Animals_With_AnimalId_Param()
        {
            await _test_AnimalController.Get(_animalId, _userId);
            _moq_AnimalDbContext.Verify(
                moq => moq.Animals.FindAsync(It.Is<Guid>(value => value == _animalId)));
        }

        [Test]
        public async Task Get_Returns_Unauthorised_When_UserId_Does_Not_Match_Animals()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Get(_animalId, _randomGuid);

            Assert.IsInstanceOf<UnauthorizedResult>(result);
        }

        [Test]
        public async Task Get_Returns_Expected_Animal_Data()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Get(_animalId, _userId);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;
            Assert.AreEqual(_animal, jsonResult.Value);

        }

        [Test]
        public async Task Get_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalDbSet = new Mock<DbSet<Animal>>();
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            var result = await _test_AnimalController.Get(_animalId, _userId);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        #endregion

        #region Get All Tests

        [Test]
        public void GetAll_Calls_Set_On_Animal_From_DbContext()
        {
            _test_AnimalController.Get();
            _moq_AnimalDbContext.Verify(moq => moq.Animals);
        }

        [Test]
        public void GetAll_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalDbContext.Setup(moq => moq.Animals).Returns<DbSet<Animal>>(null);
            var result = _test_AnimalController.Get();
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        #endregion
        #region Post Tests

        [Test]
        public async Task Post_With_Invalid_AnimalTypeId_Returns_BadRequest_StatusCode()
        {
            var invalidAnimal = new Animal()
            {
                AnimalTypeId = Guid.Empty,
                UserId = _userId
            };
            var result = await _test_AnimalController.Post(invalidAnimal, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }
        [Test]
        public async Task Post_With_Invalid_UserId_In_Animal_Returns_BadRequest_StatusCode()
        {
            var invalidAnimal = new Animal()
            {
                AnimalTypeId = _animalTypeId,
                UserId = Guid.Empty
            };
            var result = await _test_AnimalController.Post(invalidAnimal, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_With_Null_Animal_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Post(null, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_With_Invalid_UserId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Post(_animal, _emptyGuid);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_With_null_UserId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Post(_animal, null);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_With_different_Animal_UserId_To_UserId_Returns_Unauthorised_StatusCode()
        {
            var result = await _test_AnimalController.Post(_animal, _randomGuid);
            Assert.IsInstanceOf<UnauthorizedResult>(result);
            var statusCodeResult = (UnauthorizedResult)result;
            Assert.AreEqual((int)HttpStatusCode.Unauthorized, statusCodeResult.StatusCode);
        }
        [Test]
        public async Task Post_Calls_FindAsync_For_AnimalTypes_From_DbContext()
        {
            await _test_AnimalController.Post(_animal, _userId);
            _moq_AnimalDbContext.Verify(moq => moq.Animals.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Post_Calls_FindAsync_For_AnimalTypes_With_AnimalId_Param()
        {
            await _test_AnimalController.Post(_animal, _userId);
            _moq_AnimalDbContext.Verify(
                moq => moq.Animals.FindAsync(It.Is<Guid>(value => value == _animal.Id)));
        }

        [Test]
        public async Task Post_Returns_Conflict_Response_When_AnimalType_With_Id_Is_Found()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Post(_animal, _userId);

            Assert.IsInstanceOf<ConflictResult>(result);
            var conflictResult = (ConflictResult)result;
            Assert.AreEqual((int)HttpStatusCode.Conflict, conflictResult.StatusCode);

        }
        [Test]
        public async Task Post_Returns_Animal_With_Correct_Id()
        {
            var result = await _test_AnimalController.Post(_animal, _userId);

            Assert.IsAssignableFrom<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<Animal>(jsonResult.Value);
            var animalResult = (Animal)jsonResult.Value;
            Assert.AreEqual(_animal.Id, animalResult.Id);
        }

        [Test]
        public async Task Post_Returns_Animal_With_Correct_UserId()
        {
            var result = await _test_AnimalController.Post(_animal, _userId);

            Assert.IsAssignableFrom<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<Animal>(jsonResult.Value);
            var animalResult = (Animal)jsonResult.Value;
            Assert.AreEqual(_animal.UserId, animalResult.UserId);
        }

        [Test]
        public async Task Post_Returns_Animal_With_Correct_AnimalTypeId()
        {
            var result = await _test_AnimalController.Post(_animal, _userId);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<Animal>(jsonResult.Value);
            var animalResult = (Animal)jsonResult.Value;
            Assert.AreEqual(_animal.AnimalTypeId, animalResult.AnimalTypeId);
        }

        [Test]
        public async Task Post_Calls_DbContext_Animals_Add()
        {
            var result = await _test_AnimalController.Post(_animal, _userId);

            _moq_AnimalDbContext.Verify(
                moq => moq.Animals.Add(It.IsAny<Animal>()));
        }

        [Test]
        public async Task Post_Calls_DbContext_SaveChangesAsync()
        {
            var result = await _test_AnimalController.Post(_animal, _userId);

            _moq_AnimalDbContext.Verify(
                moq => moq.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task Post_Returns_InternalServerError_When_SaveChanges_Throws_Exception()
        {
            _moq_AnimalDbContext.Setup(animal => animal.SaveChangesAsync(It.IsAny<CancellationToken>())).Throws(new Exception());
            var result = await _test_AnimalController.Post(_animal, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_Returns_InternalServerError_When_Update_Throws_Exception()
        {
            _moq_AnimalDbSet.Setup(animal => animal.Add(It.IsAny<Animal>())).Throws(new Exception());
            var result = await _test_AnimalController.Post(_animal, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        #endregion

        #region Put Tests

        [Test]
        public async Task Put_With_Invalid_Animal_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Put(null, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Put_With_Invalid_UserId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Put(_putAnimal, _emptyGuid);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Put_With_null_UserId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Put(_putAnimal, null);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Put_With_Different_UserId_To_Animal_UserId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Put(_putAnimal, _randomGuid);
            Assert.IsInstanceOf<UnauthorizedResult>(result);
        }

        [Test]
        public async Task Put_Calls_FindAsync_For_Animals_From_DbContext()
        {
            await _test_AnimalController.Put(_putAnimal, _userId);
            _moq_AnimalDbContext.Verify(moq => moq.Animals.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Put_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalDbSet = new Mock<DbSet<Animal>>();
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            var result = await _test_AnimalController.Put(_putAnimal, _userId);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Put_Calls_FindAsync_For_Animals_With_AnimalId_Param()
        {
            await _test_AnimalController.Put(_putAnimal, _userId);
            _moq_AnimalDbContext.Verify(
                moq => moq.Animals.FindAsync(It.Is<Guid>(value => value == _putAnimal.Id)));
        }

        [Test]
        public async Task Put_Returns_Animal_With_Modifed_LastUpdated()
        {
            var test_lastUpdated = DateTime.Now.AddHours(-2);
            _putAnimal.LastUpdated = test_lastUpdated;

            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Put(_putAnimal, _userId);

            Assert.IsAssignableFrom<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<Animal>(jsonResult.Value);
            var animalResult = (Animal)jsonResult.Value;
            Assert.AreNotEqual(test_lastUpdated, animalResult.LastUpdated);
        }
        [Test]
        public async Task Put_Returns_Animal_With_Expected_Values()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_putAnimal);
            var result = await _test_AnimalController.Put(_putAnimal, _userId);

            Assert.IsAssignableFrom<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<Animal>(jsonResult.Value);
            var animalResult = (Animal)jsonResult.Value;
            Assert.AreEqual(_putAnimal.UserId, animalResult.UserId);
            Assert.AreEqual(_putAnimal.Id, animalResult.Id);
            Assert.AreEqual(_putAnimal.AnimalTypeId, animalResult.AnimalTypeId);
            Assert.AreEqual(_putAnimal.FoodSatisfaction, animalResult.FoodSatisfaction);
            Assert.AreEqual(_putAnimal.Happyness, animalResult.Happyness);
            Assert.AreEqual(_putAnimal.LastUpdated, animalResult.LastUpdated);
        }

        [Test]
        public async Task Put_Calls_DbContext_Animals_Update()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Put(_putAnimal, _userId);

            _moq_AnimalDbContext.Verify(
                moq => moq.Animals.Update(It.IsAny<Animal>()));
        }

        [Test]
        public async Task Put_Calls_DbContext_SaveChangesAsync()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Put(_putAnimal, _userId);

            _moq_AnimalDbContext.Verify(
                moq => moq.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task Put_Returns_InternalServerError_When_SaveChanges_Throws_Exception()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            _moq_AnimalDbContext.Setup(animal => animal.SaveChangesAsync(It.IsAny<CancellationToken>())).Throws(new Exception());
            var result = await _test_AnimalController.Put(_putAnimal, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Put_Returns_InternalServerError_When_Update_Throws_Exception()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            _moq_AnimalDbSet.Setup(animal => animal.Update(It.IsAny<Animal>())).Throws(new Exception());
            var result = await _test_AnimalController.Put(_putAnimal, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }
        #endregion

        #region Delete Tests

        [Test]
        public async Task Delete_With_Null_Id_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Delete(null, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_With_Invalid_AnimalId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Delete(_emptyGuid, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_With_Invalid_UserId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Delete(_animalId, _emptyGuid);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_With_null_UserId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalController.Delete(_animalId, null);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }
        [Test]
        public async Task Delete_Calls_FindAsync_For_Animals_From_DbContext()
        {
            await _test_AnimalController.Delete(_animalId, _userId);
            _moq_AnimalDbContext.Verify(moq => moq.Animals.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Delete_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            var result = await _test_AnimalController.Delete(_animalId, _userId);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Delete_Calls_FindAsync_For_Animals_With_AnimalId_Param()
        {
            await _test_AnimalController.Delete(_animalId, _userId);
            _moq_AnimalDbContext.Verify(
                moq => moq.Animals.FindAsync(It.Is<Guid>(value => value == _animalId)));
        }

        [Test]
        public async Task Delete_With_Different_UserId_To_Animal_UserId_Returns_BadRequest_StatusCode()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Delete(_animalId, _randomGuid);
            Assert.IsInstanceOf<UnauthorizedResult>(result);
        }

        [Test]
        public async Task Delete_Returns_Status_Ok()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Delete(_animalId, _userId);

            Assert.IsInstanceOf<StatusCodeResult>(result);
            var jsonResult = (StatusCodeResult)result;
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.OK, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_Calls_DbContext_Animals_Update()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Delete(_animal.Id, _userId);

            _moq_AnimalDbContext.Verify(
                moq => moq.Animals.Remove(It.Is<Animal>(animal => animal == _animal)));
        }

        [Test]
        public async Task Delete_Calls_DbContext_SaveChangesAsync()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            var result = await _test_AnimalController.Delete(_animal.Id, _userId);

            _moq_AnimalDbContext.Verify(
                moq => moq.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task Delete_Returns_InternalServerError_When_SaveChanges_Throws_Exception()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            _moq_AnimalDbContext.Setup(animal => animal.SaveChangesAsync(It.IsAny<CancellationToken>())).Throws(new Exception());
            var result = await _test_AnimalController.Delete(_animal.Id, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_Returns_InternalServerError_When_Update_Throws_Exception()
        {
            _moq_AnimalDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animal);
            _moq_AnimalDbSet.Setup(animal => animal.Remove(It.IsAny<Animal>())).Throws(new Exception());
            var result = await _test_AnimalController.Delete(_animal.Id, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }
        #endregion
    }
}
