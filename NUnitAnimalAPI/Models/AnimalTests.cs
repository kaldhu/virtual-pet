﻿using AnimalAPI.Models;
using NUnit.Framework;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NUnit_AnimalAPI.Models
{
    [ExcludeFromCodeCoverage]
    public class AnimalTests
    {

        private Animal _test_Animal;

        [SetUp]
        public void Setup()
        {
            _test_Animal = new Animal();
        }

        #region Constructor test
         
        [Test]
        public void Constructor_success()
        {
            Assert.DoesNotThrow((() => new Animal()));
        }

        #endregion

        #region Properties tests

        [Test]
        public void Contains_Properties_As_Expected()
        {
            Assert.IsInstanceOf<Guid>(_test_Animal.Id);
            Assert.IsInstanceOf<Guid>(_test_Animal.UserId);
            Assert.IsInstanceOf<Guid>(_test_Animal.AnimalTypeId);
            Assert.IsInstanceOf(typeof(int), _test_Animal.Happyness);
            Assert.IsInstanceOf(typeof(int), _test_Animal.FoodSatisfaction);
            Assert.IsInstanceOf(typeof(DateTime), _test_Animal.LastUpdated);
        }

        [Test]
        public void Defaults_Properties_Set_As_Expected()
        {
            var defaultHappyness = 0;
            var defaultFoodSatisfaction = 0;
            Assert.AreNotEqual(Guid.Empty, _test_Animal.Id);
            Assert.AreEqual(Guid.Empty, _test_Animal.AnimalTypeId);
            Assert.AreEqual(Guid.Empty, _test_Animal.UserId);
            Assert.AreEqual(defaultHappyness, _test_Animal.Happyness);
            Assert.AreEqual(defaultFoodSatisfaction, _test_Animal.FoodSatisfaction);
            Assert.AreNotEqual(DateTime.MinValue, _test_Animal.LastUpdated);
        }

        #region Happyness test

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-50)]
        [TestCase(-100)]
        [TestCase(-101)]
        [TestCase(1)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(101)]
        public void Happyness_Can_Be_Set_To_Valid_Int(int testValue)
        {
            _test_Animal.Happyness = testValue;
            Assert.AreEqual(testValue, _test_Animal.Happyness);
        }

        #endregion

        #region FoodSatisfaction tests

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-50)]
        [TestCase(-100)]
        [TestCase(-101)]
        [TestCase(1)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(101)]
        public void FoodSatisfaction_Can_Be_Set_To_Valid_Int(int testValue)
        {
            _test_Animal.FoodSatisfaction = testValue;
            Assert.AreEqual(testValue, _test_Animal.FoodSatisfaction);
        }
        #endregion

        #region LastUpdated tests

        [Test]
        public void LastUpdated_Set_To_Expected_Value()
        {
            var test_lastUpdated = DateTime.Now;
            _test_Animal.LastUpdated = test_lastUpdated;
            Assert.AreEqual(test_lastUpdated, _test_Animal.LastUpdated);
        }

        #endregion

        #region UserId tests

        [Test]
        public void UserId_Set_To_Expected_Value()
        {
            var testUserId = Guid.NewGuid();
            _test_Animal.UserId = testUserId;
            Assert.AreEqual(testUserId, _test_Animal.UserId);
        }

        #endregion
        #region Id tests

        [Test]
        public void Id_Set_To_Expected_Value()
        {
            var testId = Guid.NewGuid();
            _test_Animal.Id = testId;
            Assert.AreEqual(testId, _test_Animal.Id);
        }

        #endregion

        #region AnimalTypeId tests

        [Test]
        public void AnimalTypeId_Set_To_Expected_Value()
        {
            var testUserId = Guid.NewGuid();
            _test_Animal.AnimalTypeId = testUserId;
            Assert.AreEqual(testUserId, _test_Animal.AnimalTypeId);
        }

        #endregion
        
        #endregion
    }
}
