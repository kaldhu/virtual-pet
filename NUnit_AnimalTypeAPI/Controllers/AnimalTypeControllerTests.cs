﻿using AnimalTypeAPI.Controllers;
using AnimalTypeAPI.Data;
using AnimalTypeAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NUnit_AnimalTypeAPI.Controllers
{
    [ExcludeFromCodeCoverage]
    public class AnimalTypeControllerTests
    {
        private Mock<IAnimalTypeDbContext> _moq_AnimalTypeDbContext;
        private Mock<DbSet<AnimalType>> _moq_AnimalTypeDbSet;
        private AnimalTypeController _test_AnimalTypeController;
       
        private Guid _animalId;
        private Guid _randomGuid;
        private Guid _emptyGuid;
        private AnimalType _animalType;
        private AnimalType _putAnimalType;
        private int _happynessGain;
        private int _happynessLoss;
        private int _foodSatisfactionGain;
        private int _foodSatisfactionLoss;
        private List<AnimalType> _animalTypeData;

        [SetUp]
        public void Setup()
        {
            _moq_AnimalTypeDbContext = new Mock<IAnimalTypeDbContext>();
            _moq_AnimalTypeDbSet = new Mock<DbSet<AnimalType>>();
            _moq_AnimalTypeDbContext.Setup(moq => moq.AnimalTypes).Returns(_moq_AnimalTypeDbSet.Object);
            _test_AnimalTypeController = new AnimalTypeController(_moq_AnimalTypeDbContext.Object);

            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            _emptyGuid = Guid.Empty;
            _randomGuid = Guid.NewGuid();
            _animalId = Guid.NewGuid();
            _happynessGain = 1;
            _happynessLoss = 2;
            _foodSatisfactionGain = 3;
            _foodSatisfactionLoss = 4;

            _animalType = new AnimalType()
            {
                Id = _animalId,
                Name = "Test"
            };

            _putAnimalType = new AnimalType()
            {
                Id = _animalId,
                Name = "Test",
                HappynessGain = _happynessGain,
                HappynessLoss = _happynessLoss,
                FoodSatisfactionGain = _foodSatisfactionGain,
                FoodSatisfactionLoss = _foodSatisfactionLoss
            };
            _animalTypeData = new List<AnimalType>() {
                _putAnimalType
            }; 
        }

        #region Constructor Tests

        [Test]
        public void Constructor_Success()
        {
            Assert.DoesNotThrow(() => new AnimalTypeController(_moq_AnimalTypeDbContext.Object));
        }

        [Test]
        public void Constructor_With_Invalid_DbContext_Param_Throws_Exception()
        {
            Assert.Throws<InvalidOperationException>(() => new AnimalTypeController(null));
        }

        [Test]
        public void Constructor_Inherits_Controller()
        {
            Assert.IsInstanceOf<Controller>(_test_AnimalTypeController);
        }

        #endregion

        #region Get All Tests

        [Test]
        public void GetAll_Calls_Set_On_AnimalType_From_DbContext()
        {
            _test_AnimalTypeController.Get();
            _moq_AnimalTypeDbContext.Verify(moq => moq.AnimalTypes);
        }

        [Test]
        public void GetAll_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalTypeDbContext.Setup(moq => moq.AnimalTypes).Returns<DbSet<AnimalType>>(null);
            var result = _test_AnimalTypeController.Get();
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        #endregion

        #region Get by Id Tests

        [Test]
        public async Task Get_With_Invalid_Id_Params_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalTypeController.Get(Guid.Empty);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Get_Calls_FindAsync_For_AnimalTypes_From_DbContext()
        {
            await _test_AnimalTypeController.Get(_randomGuid);
            _moq_AnimalTypeDbContext.Verify(moq => moq.AnimalTypes.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Get_Calls_FindAsync_For_AnimalTypes_With_AnimalId_Param()
        {
            await _test_AnimalTypeController.Get(_animalId);
            _moq_AnimalTypeDbContext.Verify(
                moq => moq.AnimalTypes.FindAsync(It.Is<Guid>(value => value == _animalId)));
        }

        [Test]
        public async Task Get_Returns_Expected_AnimalType_Data()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Get(_animalId);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;
            Assert.AreEqual(_animalType, jsonResult.Value);

        }

        [Test]
        public async Task Get_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            var result = await _test_AnimalTypeController.Get(_animalId);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        #endregion

        #region Post Tests

        [Test]
        public async Task Post_With_Invalid_Id_Returns_BadRequest_StatusCode()
        {
            var invalidAnimalType = new AnimalType()
            {
                Id = Guid.Empty
            };
            var result = await _test_AnimalTypeController.Post(invalidAnimalType);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }
        [Test]
        public async Task Post_With_Null_AnimalType_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalTypeController.Post(null);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_Calls_FindAsync_For_AnimalTypes_From_DbContext()
        {
            await _test_AnimalTypeController.Post(_animalType);
            _moq_AnimalTypeDbContext.Verify(moq => moq.AnimalTypes.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Post_Calls_FindAsync_For_AnimalTypes_With_AnimalId_Param()
        {
            await _test_AnimalTypeController.Post(_animalType);
            _moq_AnimalTypeDbContext.Verify(
                moq => moq.AnimalTypes.FindAsync(It.Is<Guid>(value => value == _animalType.Id)));
        }

        [Test]
        public async Task Post_Returns_Conflict_Response_When_AnimalType_With_Id_Is_Found()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Post(_animalType);

            Assert.IsInstanceOf<ConflictResult>(result);
            var conflictResult = (ConflictResult)result;
            Assert.AreEqual((int)HttpStatusCode.Conflict, conflictResult.StatusCode);
        }

        [Test]
        public async Task Post_Returns_AnimalType_With_Correct_Id()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<AnimalType>(jsonResult.Value);
            var animalResult = (AnimalType)jsonResult.Value;
            Assert.AreEqual(_animalType.Id, animalResult.Id);
        }
        [Test]
        public async Task Post_Returns_AnimalType_With_Correct_Name()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<AnimalType>(jsonResult.Value);
            var animalResult = (AnimalType)jsonResult.Value;
            Assert.AreEqual(_animalType.Name, animalResult.Name);
        }

        [Test]
        public async Task Post_Returns_AnimalType_With_Correct_HappynessGain()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<AnimalType>(jsonResult.Value);
            var animalResult = (AnimalType)jsonResult.Value;
            Assert.AreEqual(_animalType.HappynessGain, animalResult.HappynessGain);
        }
        [Test]
        public async Task Post_Returns_AnimalType_With_Correct_HappynessLoss()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<AnimalType>(jsonResult.Value);
            var animalResult = (AnimalType)jsonResult.Value;
            Assert.AreEqual(_animalType.HappynessLoss, animalResult.HappynessLoss);
        }
        [Test]
        public async Task Post_Returns_AnimalType_With_Correct_FoodSatisfactionGain()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<AnimalType>(jsonResult.Value);
            var animalResult = (AnimalType)jsonResult.Value;
            Assert.AreEqual(_animalType.FoodSatisfactionGain, animalResult.FoodSatisfactionGain);
        }
        [Test]
        public async Task Post_Returns_AnimalType_With_Correct_FoodSatisfactionLoss()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<AnimalType>(jsonResult.Value);
            var animalResult = (AnimalType)jsonResult.Value;
            Assert.AreEqual(_animalType.FoodSatisfactionLoss, animalResult.FoodSatisfactionLoss);
        }

        [Test]
        public async Task Post_Calls_DbContext_AnimalTypes_Add()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            _moq_AnimalTypeDbContext.Verify(
                moq => moq.AnimalTypes.Add(It.IsAny<AnimalType>()));
        }

        [Test]
        public async Task Post_Calls_DbContext_SaveChangesAsync()
        {
            var result = await _test_AnimalTypeController.Post(_animalType);

            _moq_AnimalTypeDbContext.Verify(
                moq => moq.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task Post_Returns_InternalServerError_When_SaveChanges_Throws_Exception()
        {
            _moq_AnimalTypeDbContext.Setup(animal => animal.SaveChangesAsync(It.IsAny<CancellationToken>())).Throws(new Exception());
            var result = await _test_AnimalTypeController.Post(_animalType);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_Returns_InternalServerError_When_Update_Throws_Exception()
        {
            _moq_AnimalTypeDbSet.Setup(animal => animal.Add(It.IsAny<AnimalType>())).Throws(new Exception());
            var result = await _test_AnimalTypeController.Post(_animalType);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        #endregion

        #region Put Tests

        [Test]
        public async Task Put_With_Invalid_AnimalType_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalTypeController.Put(null);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Put_Calls_FindAsync_For_AnimalTypes_From_DbContext()
        {
            await _test_AnimalTypeController.Put(_putAnimalType);
            _moq_AnimalTypeDbContext.Verify(moq => moq.AnimalTypes.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Put_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalTypeDbSet = new Mock<DbSet<AnimalType>>();
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            var result = await _test_AnimalTypeController.Put(_putAnimalType);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Put_Calls_FindAsync_For_Animals_With_Id_Param()
        {
            await _test_AnimalTypeController.Put(_putAnimalType);
            _moq_AnimalTypeDbContext.Verify(
                moq => moq.AnimalTypes.FindAsync(It.Is<Guid>(value => value == _putAnimalType.Id)));
        }

        [Test]
        public async Task Put_Returns_AnimalType_With_Expected_Values()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Put(_putAnimalType);

            Assert.IsInstanceOf<JsonResult>(result);
            var jsonResult = (JsonResult)result;

            Assert.IsNotNull(jsonResult.Value);
            Assert.IsInstanceOf<AnimalType>(jsonResult.Value);
            var animalTypeResult = (AnimalType)jsonResult.Value;
            Assert.AreEqual(_putAnimalType.Id, animalTypeResult.Id);
            Assert.AreEqual(_putAnimalType.Name, animalTypeResult.Name);
            Assert.AreEqual(_putAnimalType.FoodSatisfactionLoss, animalTypeResult.FoodSatisfactionLoss);
            Assert.AreEqual(_putAnimalType.FoodSatisfactionGain, animalTypeResult.FoodSatisfactionGain);
            Assert.AreEqual(_putAnimalType.HappynessGain, animalTypeResult.HappynessGain);
            Assert.AreEqual(_putAnimalType.HappynessLoss, animalTypeResult.HappynessLoss);
        }

        [Test]
        public async Task Put_Calls_DbContext_AnimalTypes_Update()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Put(_putAnimalType);

            _moq_AnimalTypeDbContext.Verify(
                moq => moq.AnimalTypes.Update(It.IsAny<AnimalType>()));
        }

        [Test]
        public async Task Put_Calls_DbContext_SaveChangesAsync()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Put(_putAnimalType);

            _moq_AnimalTypeDbContext.Verify(
                moq => moq.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task Put_Returns_InternalServerError_When_SaveChanges_Throws_Exception()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            _moq_AnimalTypeDbContext.Setup(animal => animal.SaveChangesAsync(It.IsAny<CancellationToken>())).Throws(new Exception());
            var result = await _test_AnimalTypeController.Put(_putAnimalType);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Put_Returns_InternalServerError_When_Update_Throws_Exception()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            _moq_AnimalTypeDbSet.Setup(animal => animal.Update(It.IsAny<AnimalType>())).Throws(new Exception());
            var result = await _test_AnimalTypeController.Put(_putAnimalType);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }
        #endregion

        #region Delete Tests

        [Test]
        public async Task Delete_With_Null_Id_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalTypeController.Delete(null);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_With_Invalid_AnimalId_Returns_BadRequest_StatusCode()
        {
            var result = await _test_AnimalTypeController.Delete(_emptyGuid);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_Calls_FindAsync_For_AnimalTypes_From_DbContext()
        {
            await _test_AnimalTypeController.Delete(_animalId);
            _moq_AnimalTypeDbContext.Verify(moq => moq.AnimalTypes.FindAsync(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Delete_Returns_NotFound_StatusCode_When_FindAsync_Returns_Null()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).Returns(null);
            var result = await _test_AnimalTypeController.Delete(_animalId);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Delete_Calls_FindAsync_For_AnimalTypes_With_AnimalId_Param()
        {
            await _test_AnimalTypeController.Delete(_animalId);
            _moq_AnimalTypeDbContext.Verify(
                moq => moq.AnimalTypes.FindAsync(It.Is<Guid>(value => value == _animalId)));
        }

        [Test]
        public async Task Delete_Returns_Status_Ok()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Delete(_animalId);

            Assert.IsInstanceOf<StatusCodeResult>(result);
            var jsonResult = (StatusCodeResult)result;
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.OK, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_Calls_DbContext_AnimalTypes_Update()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Delete(_animalId);

            _moq_AnimalTypeDbContext.Verify(
                moq => moq.AnimalTypes.Remove(It.Is<AnimalType>(animal => animal == _animalType)));
        }

        [Test]
        public async Task Delete_Calls_DbContext_SaveChangesAsync()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            var result = await _test_AnimalTypeController.Delete(_animalId);

            _moq_AnimalTypeDbContext.Verify(
                moq => moq.SaveChangesAsync(It.IsAny<CancellationToken>()));
        }

        [Test]
        public async Task Delete_Returns_InternalServerError_When_SaveChanges_Throws_Exception()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            _moq_AnimalTypeDbContext.Setup(animal => animal.SaveChangesAsync(It.IsAny<CancellationToken>())).Throws(new Exception());
            var result = await _test_AnimalTypeController.Delete(_animalId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_Returns_InternalServerError_When_Update_Throws_Exception()
        {
            _moq_AnimalTypeDbSet.Setup(moq => moq.FindAsync(It.IsAny<Guid>())).ReturnsAsync(_animalType);
            _moq_AnimalTypeDbSet.Setup(animal => animal.Remove(It.IsAny<AnimalType>())).Throws(new Exception());
            var result = await _test_AnimalTypeController.Delete(_animalId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.InternalServerError, statusCodeResult.StatusCode);
        }
        #endregion
    }
}
