﻿using AnimalTypeAPI.Data;
using AnimalTypeAPI.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Diagnostics.CodeAnalysis;

namespace NUnit_AnimalTypeAPI.Data
{
    [ExcludeFromCodeCoverage]
    public class AnimalDbContextTests
    {
        AnimalTypeDbContext _test_AnimalDbContext;

        [SetUp]
        public void Setup()
        {
            DbContextOptions<AnimalTypeDbContext> dbContextOptions = new DbContextOptions<AnimalTypeDbContext>();
            _test_AnimalDbContext = new AnimalTypeDbContext(dbContextOptions);
        }

        [Test]
        public void Constructor_success()
        {
            AnimalTypeDbContext test_AnimalDbContext;
            DbContextOptions<AnimalTypeDbContext> test_dbContextOptions = new DbContextOptions<AnimalTypeDbContext>();
            Assert.DoesNotThrow((() => test_AnimalDbContext = new AnimalTypeDbContext(test_dbContextOptions)));
        }

        [Test]
        public void Inherits_DbContext()
        {
            Assert.IsInstanceOf(typeof(DbContext), _test_AnimalDbContext);
        }

        [Test]
        public void Contains_DBSet_Of_Animals()
        {
            Assert.IsInstanceOf(typeof(DbSet<AnimalType>), _test_AnimalDbContext.AnimalTypes);
        }
    }
}
