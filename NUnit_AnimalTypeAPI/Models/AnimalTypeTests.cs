﻿using AnimalTypeAPI.Models;
using NUnit.Framework;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NUnit_AnimalAPI.Models
{
    [ExcludeFromCodeCoverage]
    public class AnimalTypeTests
    {

        private AnimalType _test_Animal;

        [SetUp]
        public void Setup()
        {
            _test_Animal = new AnimalType();
        }

        #region Constructor test
         
        [Test]
        public void Constructor_success()
        {
            Assert.DoesNotThrow((() => new AnimalType()));
        }

        #endregion

        #region Properties tests

        [Test]
        public void Contains_Properties_As_Expected()
        {
            Assert.IsInstanceOf<Guid>(_test_Animal.Id);
            Assert.IsInstanceOf(typeof(string), _test_Animal.Name);
            Assert.IsInstanceOf(typeof(int), _test_Animal.HappynessGain);
            Assert.IsInstanceOf(typeof(int), _test_Animal.HappynessLoss);
            Assert.IsInstanceOf(typeof(int), _test_Animal.FoodSatisfactionGain);
            Assert.IsInstanceOf(typeof(int), _test_Animal.FoodSatisfactionLoss);
        }

        [Test]
        public void Defaults_Properties_Set_As_Expected()
        {
            var defaultHappynessGain = 1;
            var defaultHappynessLoss = 1;
            var defaultFoodSatisfactionGain = 1;
            var defaultFoodSatisfactionLoss = 1;
            var defaultName = "Unknown";
            Assert.AreNotEqual(Guid.Empty, _test_Animal.Id);
            Assert.AreEqual(defaultName, _test_Animal.Name);
            Assert.AreEqual(defaultHappynessGain, _test_Animal.HappynessGain);
            Assert.AreEqual(defaultHappynessLoss, _test_Animal.HappynessLoss);
            Assert.AreEqual(defaultFoodSatisfactionGain, _test_Animal.FoodSatisfactionGain);
            Assert.AreEqual(defaultFoodSatisfactionLoss, _test_Animal.FoodSatisfactionLoss);
        }

        #region Happyness test

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-50)]
        [TestCase(-100)]
        [TestCase(-101)]
        [TestCase(1)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(101)]
        public void HappynessGain_Can_Be_Set_To_Valid_Int(int testValue)
        {
            _test_Animal.HappynessGain = testValue;
            Assert.AreEqual(testValue, _test_Animal.HappynessGain);
        }
        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-50)]
        [TestCase(-100)]
        [TestCase(-101)]
        [TestCase(1)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(101)]
        public void HappynessLoss_Can_Be_Set_To_Valid_Int(int testValue)
        {
            _test_Animal.HappynessLoss = testValue;
            Assert.AreEqual(testValue, _test_Animal.HappynessLoss);
        }

        #endregion

        #region FoodSatisfaction tests

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-50)]
        [TestCase(-100)]
        [TestCase(-101)]
        [TestCase(1)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(101)]
        public void FoodSatisfactionLoss_Can_Be_Set_To_Valid_Int(int testValue)
        {
            _test_Animal.FoodSatisfactionLoss = testValue;
            Assert.AreEqual(testValue, _test_Animal.FoodSatisfactionLoss);
        }
        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-50)]
        [TestCase(-100)]
        [TestCase(-101)]
        [TestCase(1)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(101)]
        public void FoodSatisfactionGain_Can_Be_Set_To_Valid_Int(int testValue)
        {
            _test_Animal.FoodSatisfactionGain = testValue;
            Assert.AreEqual(testValue, _test_Animal.FoodSatisfactionGain);
        }
        #endregion

        #region Id tests

        [Test]
        public void Id_Set_To_Expected_Value()
        {
            var testUserId = Guid.NewGuid();
            _test_Animal.Id = testUserId;
            Assert.AreEqual(testUserId, _test_Animal.Id);
        }

        #endregion

        #region Name tests

        [Test]
        public void Name_Set_To_Expected_Value()
        {
            var testName = "Test Name";
            _test_Animal.Name = testName;
            Assert.AreEqual(testName, _test_Animal.Name);
        }

        #endregion
        #endregion
    }
}
