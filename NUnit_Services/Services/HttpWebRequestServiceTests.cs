﻿using NUnit.Framework;
using System;
using System.IO;
using System.Net;
using Services.Services;
using Moq;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace NUnit_Services.Services
{
    [ExcludeFromCodeCoverage]
    public class HttpWebRequestServiceTests
    {
        private Mock<HttpWebRequest> _moq_HttpWebRequest;
        private Mock<HttpWebResponse>  _moq_httpWebResponse;
        private IHttpWebRequestService _test_HttpWebRequestService;

        private HttpWebRequest _httpWebRequest;
        private Stream _stream;
        private string _aPIAddress = "https://localhost:44358/";
        private Uri _aPIAddressUri;
        private string _verbGetString = "Get";
        private string _verbPostString = "Post";
        private string _content = "This is a test";
        private string _contentType = "application/json";

        [SetUp]
        public void Setup()
        {
            _moq_HttpWebRequest = new Mock<HttpWebRequest>();
            _moq_httpWebResponse = new Mock<HttpWebResponse>();
            
            _moq_HttpWebRequest.Setup(moq => moq.GetResponseAsync()).ReturnsAsync(_moq_httpWebResponse.Object);

            _moq_httpWebResponse.Setup(moq => moq.StatusCode).Returns(HttpStatusCode.OK);
            _moq_httpWebResponse.Setup(moq => moq.GetResponseStream()).Returns(_stream);

            _stream = new MemoryStream();
            _test_HttpWebRequestService = new HttpWebRequestService();
            _aPIAddressUri = new Uri(_aPIAddress);
            _httpWebRequest = WebRequest.CreateHttp(_aPIAddress);
            _httpWebRequest.Method = _verbPostString;
        }

        #region BuildWebRequest Tests

        [Test]
        public void BuildWebRequest_Returns_HttpWebRequest()
        {
            var result = _test_HttpWebRequestService.BuildWebRequest(_aPIAddress, _verbGetString);
            Assert.IsInstanceOf<HttpWebRequest>(result);
        }

        [Test]
        public void BuildWebRequest_Returns_HttpWebRequest_With_Expected_Values()
        {
            var result = _test_HttpWebRequestService.BuildWebRequest(_aPIAddress, _verbGetString);
            Assert.AreEqual(_verbGetString, result.Method);
            Assert.AreEqual(_aPIAddressUri, result.Address);
        }

        [TestCase("")]
        [TestCase(null)]
        public void BuildWebRequest_With_Invalid_Param_Throws_Exception(string test_value)
        {
            Assert.Throws<ArgumentNullException>(()=> _test_HttpWebRequestService.BuildWebRequest(test_value, _verbGetString));
        }

        #endregion

        #region AddDataToRequest Tests

        [Test]
        public void AddDataToRequest_With_Invalid_HttpWebRequest_Param_Throws_Exception()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _test_HttpWebRequestService.AddDataToRequest(null, _verbGetString));
        }
        [Test]
        public void AddDataToRequest_With_Invalid_Data_Param_Throws_Exception()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _test_HttpWebRequestService.AddDataToRequest(_httpWebRequest, default(string)));
        }
        [Test]
        public async Task AddDataToRequest_Modifies_HttpWebReqest_Param_With_Expected_Values()
        {
            await _test_HttpWebRequestService.AddDataToRequest(_httpWebRequest, _content);

            var numberOfSpecialCharactersToAdd = 2;
            var test_contentLength = _content.Length + numberOfSpecialCharactersToAdd;

            Assert.AreEqual(test_contentLength, _httpWebRequest.ContentLength);
            Assert.AreEqual(_contentType, _httpWebRequest.ContentType);
        }

        #endregion

        #region GetWebResponse Tests

        [Test]
        public void GetWebResponse_With_Invalid_HttpWebRequest_Param_Throws_Exception()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _test_HttpWebRequestService.GetWebResponse(null));
        }

        [Test]
        public async Task GetWebResponse_Calls_GetResponse_From_HttpWebResult()
        {
            _moq_httpWebResponse.Setup(moq => moq.StatusCode).Returns(HttpStatusCode.NotFound);
            await _test_HttpWebRequestService.GetWebResponse(_moq_HttpWebRequest.Object);
            _moq_HttpWebRequest.Verify(moq => moq.GetResponseAsync());
        }

        [Test]
        public async Task GetWebResponse_Calls_GetResponseStream_From_httpWebResponse()
        {
            await _test_HttpWebRequestService.GetWebResponse(_moq_HttpWebRequest.Object);
            _moq_httpWebResponse.Verify(moq => moq.GetResponseStream());
        }

        [TestCase(HttpStatusCode.OK)]
        [TestCase(HttpStatusCode.NotFound)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.InternalServerError)]
        [TestCase(HttpStatusCode.NoContent)]
        public async Task GetWebResponse_Returns_Response_With_Expected_Status_Code(HttpStatusCode test_value)
        {
            _moq_httpWebResponse.Setup(moq => moq.StatusCode).Returns(test_value);
            var result = await _test_HttpWebRequestService.GetWebResponse(_moq_HttpWebRequest.Object);
            Assert.AreEqual(test_value, result.StatusCode);
        }
        #endregion
    }
}
