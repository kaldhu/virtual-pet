﻿using System.Net;

namespace Services.Models
{
    public class Response<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public T Data { get; set; }
    }
}
