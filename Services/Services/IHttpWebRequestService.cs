﻿using System.Net;
using System.Threading.Tasks;
using Services.Models;

namespace Services.Services
{
    public interface IHttpWebRequestService
    {
        public HttpWebRequest BuildWebRequest(string apiAddress, string httpVerb);
        public Task AddDataToRequest<T>(HttpWebRequest httpWebRequest, T data);
        public Task<Response<string>> GetWebResponse(HttpWebRequest httpWebRequest);
    }
}
