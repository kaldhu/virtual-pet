﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Services.Models;

namespace Services.Services
{
    public class HttpWebRequestService: IHttpWebRequestService
    {
        public HttpWebRequest BuildWebRequest(string apiAddress, string verb)
        {
            if (string.IsNullOrEmpty(apiAddress))
            {
                throw new ArgumentNullException();
            }

            HttpWebRequest httpWebRequest = WebRequest.CreateHttp(apiAddress);
            httpWebRequest.Method = verb;
            return httpWebRequest;
        }

        public async Task AddDataToRequest<T>(HttpWebRequest httpWebRequest, T data)
        {
            if(httpWebRequest == null || data == null)
            {
                throw new ArgumentNullException();
            }

            var jsonData = JsonConvert.SerializeObject(data);
            httpWebRequest.ContentLength = jsonData.Length;
            httpWebRequest.ContentType = "application/json";
            using(var streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync(), Encoding.ASCII))
            {
                streamWriter.Write(jsonData);
            }
        }

        public async Task<Response<string>> GetWebResponse(HttpWebRequest httpWebRequest)
        {
            if (httpWebRequest == null)
            {
                throw new ArgumentNullException();
            }

            var response = new Response<string>();
            try
            {
                var httpResponse = (HttpWebResponse)await httpWebRequest.GetResponseAsync();

                response.StatusCode = httpResponse.StatusCode;
                using (var streamIn = new StreamReader(httpResponse.GetResponseStream()))
                {
                    response.Data = await streamIn.ReadToEndAsync();
                }
            }
            catch (WebException ex)
            {
                var httpResponse = ex.Response as HttpWebResponse;
                if (httpResponse == null)
                    throw;
                response.StatusCode = httpResponse.StatusCode;
            }

            return response;
        }
    }
}
