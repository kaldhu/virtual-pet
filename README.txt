Welcome to the Virtual Pet Micro Service

A c# Asp.Net Core 3.0 application
with Nunit for Testing.

There are 4 services hosted on azure

VirtualPetAPI (public consuming API): "https://virtualpetapi.azurewebsites.net/"

AnimalProcessingAPI (Simulated private API): "https://animalprocessingapi.azurewebsites.net/"

AnimalAPI (Simulated private API): "https://animalapi.azurewebsites.net/"

AnimalTypeAPI (Simulated private API): "https://animaltypeapi.azurewebsites.net/"

The private API's would normally be hosted on a private VNet within Azure to ensure only admin can access these directly.

End users devices would connect and interact with the microservice via the Virtual Pet API, It therefore has a sub set of the functionality


NOTE: The UserID passed as a param is not validated and against any credentails (So feel free to make one up)
but it must remain consistent throughout operations for a User.

FUNCTIONS:

VirtualPetAPI - A public proxy api, that grants access to all other microservices within the system.
				Normally we would do user validation here.
			DEVNOTE: For ease of use the appsettings.json file has been configured to connect to the Azure hosted services

	"Get All Animal Type Data" [GET] {{HostName}}/api/animaltype	
		Returns Json Array of all animal type data

	"Get Animal Type Data" [GET] {{HostName}}/api/animaltype/[Guid]	
		Returns Json object of animal type data
	
	"Create Animal" [POST] {{HostName}}/api/virtualpet?animalTypeId=[Guid]&userId=[Guid]

	"Get All Animal Data" [GET] {{HostName}}/api/virtualpet?userId=[Guid]	
		Returns Json Array of all animal data

	"Get Animal Data" [GET] {{HostName}}/api/virtualpet/[Guid]/?userId=[Guid]	
		Returns Json object of animal data
	
	"Feed Animal" [PUT] {{HostName}}/api/virtualpet/[Guid]/feed?userId=[Guid]	
		Returns Json object of updated animal data

	"Stroke Animal" [PUT] {{HostName}}/api/virtualpet/[Guid]/stroke?userId=[Guid]
		Returns Json object of updated animal data

	"Delete Animal" [DELETE] {{HostName}}/api/virtualpet/[Guid]?userId=[Guid]

----------------
AnimalProcessingAPI - Private API which is responsible for performing actions on animal data using animal type data
			This microservice is also responsible for setting the Max and Min Happyness and FoodSatisfaction levels and how often they are updated.
			These values are set within the Appsettings and can be overriden within Azure but we have not done that here for simplicity.

			DEVNOTE: For ease of use the appsettings.json file has been configured to connect to the Azure hosted services


	"Sync Animal" [PUT] {{HostName}}/api/animal/[Guid]/sync?userId=[Guid]	
		Returns Json object of updated animal data

	"Feed Animal" [PUT] {{HostName}}/api/animal/[Guid]/feed?userId=[Guid]	
		Returns Json object of updated animal data

	"Stroke Animal" [PUT] {{HostName}}/api/animal/[Guid]/stroke?userId=[Guid]
		Returns Json object of updated animal data

----------------
AnimalAPI - Private api which is responsible for data storage and queries of animal.
			This is using an SQL Database via Enitity Framework, but can be changed to any DB Framework without effecting the other microservices.
			DEVNOTE: To run this locally you will need to have access to a database server and set the connection string as appropriate within the appsetting.json file

	"Create Animal" [Post] {{HostName}}/api/animal/[Guid]?userId=[Guid]
		Requires Animal Json to be passed as body:
			{
				"AnimalTypeId":"[Guid]",
				"UserId":"[Guid]",
			}
		Returns Created Animal Json

	"Get All Animals" [GET] {{HostName}}/api/animal?userId=[Guid]	
		Returns Json Array of all animal data

	"Get Animal" [GET] {{HostName}}/api/animal/[Guid]?userId=[Guid]
		Returns Json of animal data
	
	"Update Animal" [PUT] {{HostName}}/api/animal/[Guid]?userId=[Guid]
		Requires Animal Json to be passed as body:
		{
			"Id":"[Guid]",
			"AnimalTypeId":"[Guid]",
			"UserId":"[Guid]",
			"Happyness": 5,
			"FoodSatisfaction": 0
		}

	"Delete Animal" [DELETE] {{HostName}}/api/animal/[Guid]?userId=[Guid]

----------------
AnimalType API - Private api which is responsible for data storage and queries of animal type.
				This is using an SQL Database via Enitity Framework, but can be changed to any DB Framework without effecting the other microservices.
				DEVNOTE: To run this locally you will need to have access to a database server and set the connection string as appropriate within the appsetting.json file
				
	"Create AnimalType" [Post] {{HostName}}/api/animalType/[Guid]?userId=[Guid]
		Requires Animal Type Json to be passed as body:
			{
				"Name":"Rat",
				"HappynessGain":3,
				"HappynessLoss": 3,
				"FoodSatisfactionGain": 5,
				"FoodSatisfactionLoss": 5
			}
		Returns Created Animal Type Json

	"Get All Animal Types" [GET] {{HostName}}/api/animalType?userId=[Guid]	
		Returns Json Array of all animal type data

	"Get Animal Type" [GET] {{HostName}}/api/animalType/[Guid]?userId=[Guid]
		Returns Json of animal type data
	
	"Update Animal Type" [PUT] {{HostName}}/api/animalType/[Guid]?userId=[Guid]
		Requires Animal Type Json to be passed as body:
		{
			"Id":"[Guid]",
			"Name":"Rat",
			"HappynessGain":3,
			"HappynessLoss": 3,
			"FoodSatisfactionGain": 5,
			"FoodSatisfactionLoss": 5
		}

	"Delete Animal Type" [DELETE] {{HostName}}/api/animalType/[Guid]?userId=[Guid]