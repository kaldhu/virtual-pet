﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using Services.Models;
using Services.Services;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading.Tasks;
using VirtualPetAPI.Controllers;
using VirtualPetAPI.Models;

namespace NUnit_VirtualPetAPI.Controllers
{
    [ExcludeFromCodeCoverage]
    class AnimalTypeControllerTests
    {
        private Mock<HttpWebRequest> _moq_AnimalTypeHttpWebRequest;
        private Mock<IOptions<Config>> _moq_Config;
        private Mock<IHttpWebRequestService> _moq_HttpWebRequestService;
        private AnimalTypeController _test_AnimalTypeController;

        private Config _config;
        private Guid _randomGuid = Guid.NewGuid();
        private Guid _animalTypeId = Guid.NewGuid();
        private Response<string> _animalTypeResponseString = new Response<string>();
        private HttpStatusCode _okStatusCode = HttpStatusCode.OK;
        private string _contentData = "Content Data";

        [SetUp]
        public void Setup()
        {
            _moq_AnimalTypeHttpWebRequest = new Mock<HttpWebRequest>();
            _moq_HttpWebRequestService = new Mock<IHttpWebRequestService>();
            _moq_Config = new Mock<IOptions<Config>>();

            _config = new Config();
            _config.AnimalAPIAddress = "TestAnimalAPIAddress";
            _config.AnimalProcessingAPIAddress = "TestAnimalProcessingAPIAddress";
            _config.AnimalTypeAPIAddress = "TestAnimalTypeAPIAddress";


            _animalTypeResponseString.StatusCode = _okStatusCode;
            _animalTypeResponseString.Data = _contentData;
            _moq_Config.Setup(moq => moq.Value).Returns(_config);

            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalTypeAPIAddress)), It.IsAny<string>())).Returns(_moq_AnimalTypeHttpWebRequest.Object);
            
            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>())).ReturnsAsync(_animalTypeResponseString);
         
            _test_AnimalTypeController = new AnimalTypeController(_moq_Config.Object, _moq_HttpWebRequestService.Object);
        }

        #region Constructor Tests

        [Test]
        public void Constructor_Success()
        {
            Assert.DoesNotThrow(() => new AnimalTypeController(_moq_Config.Object, _moq_HttpWebRequestService.Object));
        }

        [Test]
        public void Constructor_With_Invalid_Config_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalTypeController(null, _moq_HttpWebRequestService.Object));
        }

        [Test]
        public void Constructor_With_Invalid_WebRequest_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalTypeController(_moq_Config.Object, null));
        }

        [Test]
        public void Constructor_Inherits_Controller()
        {
            Assert.IsInstanceOf<Controller>(_test_AnimalTypeController);
        }

        #endregion

        #region Get Tests

        [Test]
        public async Task Get_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_AnimalTypeController.Get(_randomGuid);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public async Task Get_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data()
        {
            var verb = "GET";
            await _test_AnimalTypeController.Get(_animalTypeId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalTypeAPIAddress) &&
                    v.Contains(_animalTypeId.ToString())),
                    It.Is<string>(v => v == verb)));
        }
        [Test]
        public async Task Get_Calls_httpWebRequestService_GetWebResponse()
        {
            var result = await _test_AnimalTypeController.Get(_animalTypeId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()));
        }

        [Test]
        public async Task Get_Calls_httpWebRequestService_GetWebResponse_With_Expected_Animal_HttpWebRequest()
        {
            var result = await _test_AnimalTypeController.Get(_animalTypeId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_AnimalTypeHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Get_Returns_NonOk_Status_Code_Returned_By_GetWebResponse(HttpStatusCode testValue)
        {

            _animalTypeResponseString.StatusCode = testValue;
            var result = await _test_AnimalTypeController.Get(_animalTypeId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Get_Returns_Expected_Response()
        {
            var result = await _test_AnimalTypeController.Get(_animalTypeId);
            Assert.IsInstanceOf<ContentResult>(result);
            var statusCodeResult = (ContentResult)result;
            Assert.AreEqual(_contentData, statusCodeResult.Content);
        }
        #endregion

        #region GetAll Tests

        [Test]
        public async Task GetAll_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_AnimalTypeController.Get();
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public async Task GetAll_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data()
        {
            var verb = "GET";
            await _test_AnimalTypeController.Get();
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalTypeAPIAddress)),
                    It.Is<string>(v => v == verb)));
        }
        [Test]
        public async Task GetAll_Calls_httpWebRequestService_GetWebResponse()
        {
            var result = await _test_AnimalTypeController.Get();
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()));
        }

        [Test]
        public async Task GetAll_Calls_httpWebRequestService_GetWebResponse_With_Expected_Animal_HttpWebRequest()
        {
            var result = await _test_AnimalTypeController.Get();
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_AnimalTypeHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task GetAll_Returns_NonOk_Status_Code_Returned_By_GetWebResponse(HttpStatusCode testValue)
        {

            _animalTypeResponseString.StatusCode = testValue;
            var result = await _test_AnimalTypeController.Get();
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task GetAll_Returns_Expected_Response()
        {
            var result = await _test_AnimalTypeController.Get();
            Assert.IsInstanceOf<ContentResult>(result);
            var statusCodeResult = (ContentResult)result;
            Assert.AreEqual(_contentData, statusCodeResult.Content);
        }
        #endregion
    }
}
