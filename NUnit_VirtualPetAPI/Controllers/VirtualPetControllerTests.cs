﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using Services.Models;
using Services.Services;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading.Tasks;
using VirtualPetAPI.Controllers;
using VirtualPetAPI.Models;

namespace NUnit_VirtualPetAPI.Controllers
{
    [ExcludeFromCodeCoverage]
    public class VirtualPetControllerTests
    {

        private Mock<HttpWebRequest> _moq_AnimalHttpWebRequest;
        private Mock<HttpWebRequest> _moq_ProcessSyncHttpWebRequest;
        private Mock<HttpWebRequest> _moq_ProcessFeedHttpWebRequest;
        private Mock<HttpWebRequest> _moq_ProcessStrokeHttpWebRequest;
        private Mock<HttpWebRequest> _moq_AnimalTypeHttpWebRequest;
        private Mock<IOptions<Config>> _moq_Config;
        private Mock<IHttpWebRequestService> _moq_HttpWebRequestService;
        private VirtualPetController _test_VirtualPetController;

        private HttpStatusCode _okStatusCode = HttpStatusCode.OK;
        private Config _config;
        private Guid _randomGuid = Guid.NewGuid();
        private Guid _animalId = Guid.NewGuid();
        private Guid _animalTypeId = Guid.NewGuid();
        private Guid _userId = Guid.NewGuid();
        private Response<string> _animalResponseString = new Response<string>();
        private Response<string> _animalTypeResponseString = new Response<string>();
        private Response<string> _ProcessSyncResponseString = new Response<string>();
        private Response<string> _ProcessFeedResponseString = new Response<string>();
        private Response<string> _ProcessStrokeResponseString = new Response<string>();

        private string _contentData = "Content Data";
        private string _feed = "feed";
        private string _sync = "sync";
        private string _stroke = "stroke";

        [SetUp]
        public void Setup()
        {
            _moq_AnimalTypeHttpWebRequest = new Mock<HttpWebRequest>();
            _moq_AnimalHttpWebRequest = new Mock<HttpWebRequest>();
            _moq_ProcessFeedHttpWebRequest = new Mock<HttpWebRequest>();
            _moq_ProcessSyncHttpWebRequest = new Mock<HttpWebRequest>();
            _moq_ProcessStrokeHttpWebRequest = new Mock<HttpWebRequest>();
            _moq_HttpWebRequestService = new Mock<IHttpWebRequestService>();
            _moq_Config = new Mock<IOptions<Config>>();

            _config = new Config();
            _config.AnimalAPIAddress = "TestAnimalAPIAddress";
            _config.AnimalProcessingAPIAddress = "TestAnimalProcessingAPIAddress";
            _config.AnimalTypeAPIAddress = "TestAnimalTypeAPIAddress";

            _animalResponseString.StatusCode = _okStatusCode;
            _animalResponseString.Data = _contentData;

            _animalTypeResponseString.StatusCode = _okStatusCode;
            _animalTypeResponseString.Data = _contentData;

            _ProcessSyncResponseString.StatusCode = _okStatusCode;
            _ProcessSyncResponseString.Data = _contentData;

            _ProcessFeedResponseString.StatusCode = _okStatusCode;
            _ProcessFeedResponseString.Data = _contentData;

            _ProcessStrokeResponseString.StatusCode = _okStatusCode;
            _ProcessStrokeResponseString.Data = _contentData;

            _moq_Config.Setup(moq => moq.Value).Returns(_config);

            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalAPIAddress)), It.IsAny<string>())).Returns(_moq_AnimalHttpWebRequest.Object);
            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalTypeAPIAddress)), It.IsAny<string>())).Returns(_moq_AnimalTypeHttpWebRequest.Object);
            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalProcessingAPIAddress) && v.Contains(_sync)), It.IsAny<string>())).Returns(_moq_ProcessSyncHttpWebRequest.Object);
            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalProcessingAPIAddress) && v.Contains(_feed)), It.IsAny<string>())).Returns(_moq_ProcessFeedHttpWebRequest.Object);
            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalProcessingAPIAddress) && v.Contains(_stroke)), It.IsAny<string>())).Returns(_moq_ProcessStrokeHttpWebRequest.Object);

            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v == _moq_AnimalHttpWebRequest.Object))).ReturnsAsync(_animalResponseString);
            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v == _moq_AnimalTypeHttpWebRequest.Object))).ReturnsAsync(_animalTypeResponseString);
            
            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v == _moq_ProcessSyncHttpWebRequest.Object))).ReturnsAsync(_ProcessSyncResponseString);
            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v == _moq_ProcessFeedHttpWebRequest.Object))).ReturnsAsync(_ProcessFeedResponseString);
            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v == _moq_ProcessStrokeHttpWebRequest.Object))).ReturnsAsync(_ProcessStrokeResponseString);


            _test_VirtualPetController = new VirtualPetController(_moq_Config.Object, _moq_HttpWebRequestService.Object);
        }

        #region Constructor Tests

        [Test]
        public void Constructor_Success()
        {
            Assert.DoesNotThrow(() => new VirtualPetController(_moq_Config.Object, _moq_HttpWebRequestService.Object));
        }

        [Test]
        public void Constructor_With_Invalid_Config_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new VirtualPetController(null, _moq_HttpWebRequestService.Object));
        }
        [Test]
        public void Constructor_With_Invalid_HttpWebRequest_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new VirtualPetController(_moq_Config.Object, null));
        }

        [Test]
        public void Constructor_Inherits_Controller()
        {
            Assert.IsInstanceOf<Controller>(_test_VirtualPetController);
        }

        #endregion

        #region Get By Id Tests

        [Test]
        public async Task Get_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_VirtualPetController.Get(_randomGuid, _randomGuid);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public async Task Get_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Get_Animal()
        {
            var verb = "GET";
            await _test_VirtualPetController.Get(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalAPIAddress) && 
                    v.Contains(_animalId.ToString()) && 
                    v.Contains(_userId.ToString())),
                    It.Is<string>(v => v == verb)));
        }

        [Test]
        public async Task Get_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Sync_Animal()
        {
            await _test_VirtualPetController.Get(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalProcessingAPIAddress) &&
                    v.Contains(_animalId.ToString()) &&
                    v.Contains(_userId.ToString())),
                It.IsAny<string>()));
        }

        [Test]
        public async Task Get_Calls_httpWebRequestService_GetWebResponse_Twice()
        {
            var result = await _test_VirtualPetController.Get(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()),Times.Exactly(2));
        }

        [Test]
        public async Task Get_Calls_httpWebRequestService_GetWebResponse_With_Expected_Animal_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Get(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_AnimalHttpWebRequest.Object))));
        }
        [Test]
        public async Task Get_Calls_httpWebRequestService_GetWebResponse_With_Expected_Process_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Get(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_ProcessSyncHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Get_Returns_NonOk_Status_Code_Returned_By_Animal_GetWebResponse(HttpStatusCode testValue)
        {

            _animalResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Get(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Get_Returns_NonOk_Status_Code_Returned_By_Process_GetWebResponse(HttpStatusCode testValue)
        {

            _ProcessSyncResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Get(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Get_Returns_Expected_Response()
        {
            var result = await _test_VirtualPetController.Get(_animalId, _userId);
            Assert.IsInstanceOf<ContentResult>(result);
            var statusCodeResult = (ContentResult)result;
            Assert.AreEqual(_contentData, statusCodeResult.Content);
        }
        #endregion

        #region Get All Tests

        [Test]
        public async Task GetAll_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_VirtualPetController.Get(_randomGuid);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public async Task GetAll_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data()
        {
            var verb = "GET";
            await _test_VirtualPetController.Get(_userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalAPIAddress) &&
                    v.Contains(_userId.ToString())),
                    It.Is<string>(v => v == verb)));
        }
        [Test]
        public async Task GetAll_Calls_httpWebRequestService_GetWebResponse()
        {
            var result = await _test_VirtualPetController.Get(_userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()));
        }

        [Test]
        public async Task GetAll_Calls_httpWebRequestService_GetWebResponse_With_Expected_Animal_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Get( _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_AnimalHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task GetAll_Returns_NonOk_Status_Code_Returned_By_Animal_GetWebResponse(HttpStatusCode testValue)
        {

            _animalResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Get( _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task GetAll_Returns_Expected_Response()
        {
            var result = await _test_VirtualPetController.Get(_userId);
            Assert.IsInstanceOf<ContentResult>(result);
            var statusCodeResult = (ContentResult)result;
            Assert.AreEqual(_contentData, statusCodeResult.Content);
        }

        #endregion

        #region Delete Tests

        [Test]
        public async Task Delete_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_VirtualPetController.Delete(_randomGuid, _randomGuid);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public async Task Delete_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data()
        {
            var verb = "DELETE";
            await _test_VirtualPetController.Delete(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalAPIAddress) &&
                    v.Contains(_animalId.ToString()) &&
                    v.Contains(_userId.ToString())),
                    It.Is<string>(v => v == verb)));
        }
        [Test]
        public async Task Delete_Calls_httpWebRequestService_GetWebResponse()
        {
            var result = await _test_VirtualPetController.Delete(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()));
        }

        [Test]
        public async Task Delete_Calls_httpWebRequestService_GetWebResponse_With_Expected_Animal_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Delete(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_AnimalHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Delete_Returns_NonOk_Status_Code_Returned_By_Animal_GetWebResponse(HttpStatusCode testValue)
        {

            _animalResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Delete(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Delete_Returns_Expected_Response()
        {
            var result = await _test_VirtualPetController.Delete(_animalId, _userId);
            Assert.IsInstanceOf<OkResult>(result);
        }
        #endregion

        #region Feed Tests

        [Test]
        public async Task Feed_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_VirtualPetController.Feed(_randomGuid, _randomGuid);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public async Task Feed_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Feed_Animal()
        {
            var verb = "PUT";
            await _test_VirtualPetController.Feed(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalProcessingAPIAddress) &&
                    v.Contains(_feed) &&
                    v.Contains(_animalId.ToString()) &&
                    v.Contains(_userId.ToString())),
                    It.Is<string>(v => v == verb)));
        }

        [Test]
        public async Task Feed_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Sync_Animal()
        {
            await _test_VirtualPetController.Feed(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalProcessingAPIAddress) &&
                    v.Contains(_sync) &&
                    v.Contains(_animalId.ToString()) &&
                    v.Contains(_userId.ToString())),
                It.IsAny<string>()));
        }

        [Test]
        public async Task Feed_Calls_httpWebRequestService_FeedWebResponse_Twice()
        {
            var result = await _test_VirtualPetController.Feed(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()), Times.Exactly(2));
        }

        [Test]
        public async Task Feed_Calls_httpWebRequestService_FeedWebResponse_With_Expected_Sync_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Feed(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_ProcessSyncHttpWebRequest.Object))));
        }

        [Test]
        public async Task Feed_Calls_httpWebRequestService_FeedWebResponse_With_Expected_Feed_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Feed(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_ProcessFeedHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Feed_Returns_NonOk_Status_Code_Returned_By_Animal_SyncWebResponse(HttpStatusCode testValue)
        {

            _ProcessSyncResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Feed(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Feed_Returns_NonOk_Status_Code_Returned_By_Process_FeedWebResponse(HttpStatusCode testValue)
        {

            _ProcessFeedResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Feed(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Feed_Returns_Expected_Response()
        {
            var result = await _test_VirtualPetController.Feed(_animalId, _userId);
            Assert.IsInstanceOf<ContentResult>(result);
            var statusCodeResult = (ContentResult)result;
            Assert.AreEqual(_contentData, statusCodeResult.Content);
        }
        #endregion

        #region Stroke Tests

        [Test]
        public async Task Stroke_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_VirtualPetController.Stroke(_randomGuid, _randomGuid);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public async Task Stroke_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Stroke_Animal()
        {
            var verb = "PUT";
            await _test_VirtualPetController.Stroke(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalProcessingAPIAddress) &&
                    v.Contains(_stroke) &&
                    v.Contains(_animalId.ToString()) &&
                    v.Contains(_userId.ToString())),
                    It.Is<string>(v => v == verb)));
        }

        [Test]
        public async Task Stroke_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Sync_Animal()
        {
            await _test_VirtualPetController.Stroke(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalProcessingAPIAddress) &&
                    v.Contains(_sync) &&
                    v.Contains(_animalId.ToString()) &&
                    v.Contains(_userId.ToString())),
                It.IsAny<string>()));
        }

        [Test]
        public async Task Stroke_Calls_httpWebRequestService_StrokeWebResponse_Twice()
        {
            var result = await _test_VirtualPetController.Stroke(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()), Times.Exactly(2));
        }

        [Test]
        public async Task Stroke_Calls_httpWebRequestService_StrokeWebResponse_With_Expected_Sync_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Stroke(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_ProcessSyncHttpWebRequest.Object))));
        }

        [Test]
        public async Task Stroke_Calls_httpWebRequestService_StrokeWebResponse_With_Expected_Stroke_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Stroke(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_ProcessStrokeHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Stroke_Returns_NonOk_Status_Code_Returned_By_Animal_SyncWebResponse(HttpStatusCode testValue)
        {

            _ProcessSyncResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Stroke(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Stroke_Returns_NonOk_Status_Code_Returned_By_Process_StrokeWebResponse(HttpStatusCode testValue)
        {

            _ProcessStrokeResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Stroke(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Stroke_Returns_Expected_Response()
        {
            var result = await _test_VirtualPetController.Stroke(_animalId, _userId);
            Assert.IsInstanceOf<ContentResult>(result);
            var statusCodeResult = (ContentResult)result;
            Assert.AreEqual(_contentData, statusCodeResult.Content);
        }
        #endregion

        #region Post Tests

        [Test]
        public async Task Post_Calls_IHttpWebRequestService_BuildWebRequest()
        {
            await _test_VirtualPetController.Post(_randomGuid, _randomGuid);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public async Task Post_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Post_Animal()
        {
            var verb = "POST";
            await _test_VirtualPetController.Post(_animalTypeId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalAPIAddress) &&
                    v.Contains(_userId.ToString())),
                    It.Is<string>(v => v == verb)));
        }

        [Test]
        public async Task Post_Calls_IHttpWebRequestService_BuildWebRequest_With_Expected_Data_For_Get_AnimalType()
        {
            var verb = "GET";
            await _test_VirtualPetController.Post(_animalTypeId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(
                It.Is<string>(v =>
                    v.Contains(_config.AnimalTypeAPIAddress) &&
                    v.Contains(_animalTypeId.ToString())),
                    It.Is<string>(v => v == verb)));
        }

        [Test]
        public async Task Post_Calls_httpWebRequestService_PostWebResponse_Twice()
        {
            var result = await _test_VirtualPetController.Post(_animalTypeId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()), Times.Exactly(2));
        }

        [Test]
        public async Task Post_Calls_httpWebRequestService_AnimalTypeWebResponse_With_Expected_Sync_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Post(_animalTypeId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_AnimalTypeHttpWebRequest.Object))));
        }

        [Test]
        public async Task Post_Calls_httpWebRequestService_AnimalWebResponse_With_Expected_Post_HttpWebRequest()
        {
            var result = await _test_VirtualPetController.Post(_animalTypeId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_AnimalHttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Post_Returns_NonOk_Status_Code_Returned_By_AnimalType_SyncWebResponse(HttpStatusCode testValue)
        {

            _animalTypeResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Post(_animalTypeId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Post_Returns_NonOk_Status_Code_Returned_By_Process_PostWebResponse(HttpStatusCode testValue)
        {

            _animalResponseString.StatusCode = testValue;
            var result = await _test_VirtualPetController.Post(_animalTypeId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)testValue, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Post_Returns_Expected_Response()
        {
            var result = await _test_VirtualPetController.Post(_animalTypeId, _userId);
            Assert.IsInstanceOf<ContentResult>(result);
            var statusCodeResult = (ContentResult)result;
            Assert.AreEqual(_contentData, statusCodeResult.Content);
        }
        #endregion
    }
}
