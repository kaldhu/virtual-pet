﻿using AnimalProcessingAPI.Managers;
using AnimalProcessingAPI.Managers.Interfaces;
using AnimalProcessingAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Services.Models;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NUnit_AnimalProcessingAPI.Managers
{
    [ExcludeFromCodeCoverage]
    public class AnimalManagerTests
    {

        private Mock<HttpWebRequest> _moq_HttpWebRequest;
        private Mock<IOptions<Config>> _moq_ConfigOptions;
        private Mock<IHttpWebRequestService> _moq_HttpWebRequestService;
        private AnimalManager _test_AnimalManager;

        private Guid _animalId = Guid.NewGuid();
        private Guid _userId = Guid.NewGuid();
        private Response<string> _responseString = new Response<string>();
        private Animal _animal = new Animal();
        private Config _config = new Config();
        private HttpStatusCode _statusCode = HttpStatusCode.OK;


        private int _feedAmount = 1;
        private int _strokeAmount = 1;
        private int _happyness = 5;
        private int _foodSatisfaction = 5;
        private int _happynessLoss = 1;
        private int _foodSatisfactionLoss = 1;
        private int _minHappyness = -10;
        private int _minFoodSatisfaction = -10;
        private int _maxHappyness = 10;
        private int _maxFoodSatisfaction = 10;
        private TimeSpan _timeSpanInterval = new TimeSpan(0, 1, 0);

        [SetUp]
        public void Setup()
        {
            _moq_HttpWebRequest = new Mock<HttpWebRequest>();
            _moq_ConfigOptions = new Mock<IOptions<Config>>();
            _moq_ConfigOptions.Setup(moq => moq.Value).Returns(_config);

            _config.AnimalAPIAddress = "TestAnimalAPIAddress";
            _config.LossIntervalHours = _timeSpanInterval.Hours;
            _config.LossIntervalMinutes = _timeSpanInterval.Minutes;
            _config.LossIntervalSeconds = _timeSpanInterval.Seconds;
            _config.MinFoodSatisfaction = _minFoodSatisfaction;
            _config.MinHappyness = _minHappyness;
            _config.MaxFoodSatisfaction = _maxFoodSatisfaction;
            _config.MaxHappyness = _maxHappyness;

            _moq_HttpWebRequestService = new Mock<IHttpWebRequestService>();
            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>())).Returns(_moq_HttpWebRequest.Object);
            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>())).ReturnsAsync(_responseString);

            _responseString.StatusCode = _statusCode;
            _responseString.Data = JsonConvert.SerializeObject(_animal);

             _test_AnimalManager = new AnimalManager(_moq_ConfigOptions.Object, _moq_HttpWebRequestService.Object);

            _animal.Happyness = _happyness;
            _animal.FoodSatisfaction = _foodSatisfaction;
        }

        #region Constructor Tests

        [Test]
        public void Constructor_Success()
        {
            Assert.DoesNotThrow(() => new AnimalManager(_moq_ConfigOptions.Object, _moq_HttpWebRequestService.Object));
        }

        [Test]
        public void Constructor_With_Invalid_HttpWebRequestService_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalManager(_moq_ConfigOptions.Object,null));
        }

        [Test]
        public void Constructor_With_Invalid_Config_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalManager(null, _moq_HttpWebRequestService.Object));
        }

        [Test]
        public void Constructor_Inherits_IAnimalManager()
        {
            Assert.IsInstanceOf<IAnimalManager>(_test_AnimalManager);
        }

        #endregion

        #region GetAnimal Test

        [Test]
        public async Task GetAnimal_Calls_httpWebRequestService_BuildWebRequest()
        {
            var result = await _test_AnimalManager.GetAnimal(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public async Task GetAnimal_Calls_httpWebRequestService_BuildWebRequest_With_Expected_Address()
        {

            var result = await _test_AnimalManager.GetAnimal(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalAPIAddress)), It.IsAny<string>()));
        }
        [Test]
        public async Task GetAnimal_Calls_httpWebRequestService_BuildWebRequest_With_Expected_Verb()
        {
            var expectedVerb = "GET";
            var result = await _test_AnimalManager.GetAnimal(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.Is<string>(v => v == expectedVerb)));
        }

        [Test]
        public async Task GetAnimal_Calls_httpWebRequestService_GetWebResponse()
        {
            var result = await _test_AnimalManager.GetAnimal(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()));
        }

        [Test]
        public async Task GetAnimal_Calls_httpWebRequestService_GetWebResponse_With_Expected_HttpWebRequest()
        {
            var result = await _test_AnimalManager.GetAnimal(_animalId, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_HttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task GetAnimal_Returns_NonOk_Status_Code_Returned_By_GetWebResponse(HttpStatusCode testCode)
        {

            _responseString.StatusCode = testCode;
            var result = await _test_AnimalManager.GetAnimal(_animalId, _userId);
            Assert.AreEqual(testCode, result.StatusCode);
        }

        [Test]
        public async Task GetAnimal_Returns_Expected_Data()
        { 
            var result = await _test_AnimalManager.GetAnimal(_animalId, _userId);
            Assert.AreEqual(_statusCode, result.StatusCode);
            Assert.AreEqual(_animal.Id, result.Data.Id);
            Assert.AreEqual(_animal.LastUpdated, result.Data.LastUpdated);
            Assert.AreEqual(_animal.Happyness, result.Data.Happyness);
            Assert.AreEqual(_animal.FoodSatisfaction, result.Data.FoodSatisfaction);
            Assert.AreEqual(_animal.AnimalTypeId, result.Data.AnimalTypeId);
        }
        #endregion

        #region CalculateStatLoss Tests

        [Test]
        public void CalculateStatLoss_With_Invalid_Animal_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => _test_AnimalManager.CalculateStatLoss(null, _happynessLoss, _foodSatisfactionLoss));
        }

        [TestCase(-1)]
        [TestCase(-10)]
        [TestCase(-100)]
        public void CalculateStatLoss_With_Invalid_HappynessLoss_Param_Throws_Exception(int test_value)
        {
            Assert.Throws<InvalidOperationException>(() => _test_AnimalManager.CalculateStatLoss(_animal, test_value, _foodSatisfactionLoss));
        }

        [TestCase(-1)]
        [TestCase(-10)]
        [TestCase(-100)]
        public void CalculateStatLoss_With_Invalid_Animal_FoodSatisfactionLoss_Throws_Exception(int test_value)
        {
            Assert.Throws<InvalidOperationException>(() => _test_AnimalManager.CalculateStatLoss(_animal, _happynessLoss, test_value));
        }

        [Test]
        public void CalculateStatLoss_Returns_Unchanged_Animal_If_Interval_Not_Passed()
        {
            _animal.LastUpdated = DateTime.UtcNow;
			var result = _test_AnimalManager.CalculateStatLoss(_animal, _happynessLoss, _foodSatisfactionLoss);
            Assert.AreEqual(_happyness, result.Happyness);
            Assert.AreEqual(_foodSatisfaction, result.FoodSatisfaction);
        }

        [Test]
        public void CalculateStatLoss_Returns_Updated_Animal_If_Interval_Passed()
        {
            _animal.LastUpdated = DateTime.UtcNow.Subtract(_timeSpanInterval);
            _animal.LastUpdated = _animal.LastUpdated.Subtract(_timeSpanInterval);

            var result = _test_AnimalManager.CalculateStatLoss(_animal, _happynessLoss, _foodSatisfactionLoss);
            Assert.AreNotEqual(_happyness, result.Happyness);
            Assert.AreNotEqual(_foodSatisfaction, result.FoodSatisfaction);
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(15)]
        public void CalculateStatLoss_Returns_Updated_Animal_With_Expected_Results(int numberOfExpectedIntervals)
        {
            _animal.LastUpdated = DateTime.UtcNow;
            for (int i = 0; i < numberOfExpectedIntervals; i++)
            {
                _animal.LastUpdated = _animal.LastUpdated.Subtract(_timeSpanInterval);
            }

            var result = _test_AnimalManager.CalculateStatLoss(_animal, _happynessLoss, _foodSatisfactionLoss);

            Assert.AreEqual(_happyness - (_happynessLoss* numberOfExpectedIntervals), result.Happyness);
            Assert.AreEqual(_foodSatisfaction - (_foodSatisfactionLoss * numberOfExpectedIntervals), result.FoodSatisfaction);
        }

        [Test]
        public void CalculateStatLoss_Returns_Updated_Animal_With_Expected_Results()
        {
            var happynessLoss = 5;
            var foodSatisfactionLoss = 5;
            int numberOfExpectedIntervals = 5;
            _animal.LastUpdated = DateTime.UtcNow;
            for (int i = 0; i < numberOfExpectedIntervals; i++)
            {
                _animal.LastUpdated = _animal.LastUpdated.Subtract(_timeSpanInterval);
            }

            var result = _test_AnimalManager.CalculateStatLoss(_animal, happynessLoss, foodSatisfactionLoss);

            Assert.AreEqual(_minHappyness, result.Happyness);
            Assert.AreEqual(_minFoodSatisfaction, result.FoodSatisfaction);
        }
        #endregion

        #region UpdateAnimal Test

        [Test]
        public async Task UpdateAnimal_Calls_httpWebRequestService_BuildWebRequest()
        {
            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public async Task UpdateAnimal_Calls_httpWebRequestService_BuildWebRequest_With_Expected_Address()
        {

            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalAPIAddress)), It.IsAny<string>()));
        }

        [Test]
        public async Task UpdateAnimal_Calls_httpWebRequestService_BuildWebRequest_With_Expected_Verb()
        {
            var expectedVerb = "PUT";
            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.Is<string>(v => v == expectedVerb)));
        }

        [Test]
        public async Task UpdateAnimal_Calls_httpWebRequestService_GetWebResponse()
        {
            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()));
        }

        [Test]
        public async Task UpdateAnimal_Calls_httpWebRequestService_AddDataToRequest()
        {
            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.AddDataToRequest(It.IsAny<HttpWebRequest>(), It.IsAny<Animal>()));
        }

        [Test]
        public async Task UpdateAnimal_Calls_httpWebRequestService_AddDataToRequest_With_Expected_Animal()
        {
            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.AddDataToRequest(It.IsAny<HttpWebRequest>(), It.Is<Animal>(v => v.Equals(_animal))));
        }

        [Test]
        public async Task UpdateAnimal_Calls_httpWebRequestService_GetWebResponse_With_Expected_HttpWebRequest()
        {
            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_HttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.OK)]
        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task UpdateAnimal_Returns_Status_Code_Returned_By_GetWebResponse(HttpStatusCode testCode)
        {

            _responseString.StatusCode = testCode;
            var result = await _test_AnimalManager.UpdateAnimal(_animal, _userId);
            Assert.AreEqual(testCode, result);
        }

        #endregion

        #region FeedAnimal Test

        [Test]
        public void FeedAnimal_With_Invalid_Animal_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => _test_AnimalManager.FeedAnimal(null, _feedAmount));
        }


        [Test]
        public void FeedAnimal_With_Invalid_Feed_Param_Throws_Exception()
        {
            var feedAmount = -1;
            Assert.Throws<InvalidOperationException>(() => _test_AnimalManager.FeedAnimal(_animal, feedAmount));
        }

        [Test]
        public void FeedAnimal_Returns_Same_Animal()
        {
            var result = _test_AnimalManager.FeedAnimal(_animal, _feedAmount);
            Assert.IsInstanceOf<Animal>(result);
            Assert.AreEqual(_animal.Id, result.Id);
            Assert.AreEqual(_animal.AnimalTypeId, result.AnimalTypeId);
        }

        [TestCase(1)]
        [TestCase(3)]
        [TestCase(5)]
        public void FeedAnimal_Returns_Same_Animal_With_Updated_FoodSatisfaction(int testValue)
        {
            var currentFoodSatisfaction = _animal.FoodSatisfaction;
            var result = _test_AnimalManager.FeedAnimal(_animal, testValue);
            Assert.AreEqual(currentFoodSatisfaction + testValue, result.FoodSatisfaction);
        }

        [Test]
        public void FeedAnimal_Returns_Same_Animal_With_Updated_FoodSatisfaction_But_Unchanged_Happyness()
        {
            var currentHappyness = _animal.Happyness;
            var result = _test_AnimalManager.FeedAnimal(_animal, _feedAmount);
            Assert.AreEqual(currentHappyness, result.Happyness);
        }

        #endregion

        #region StrokeAnimal Test

        [Test]
        public void StrokeAnimal_With_Invalid_Animal_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => _test_AnimalManager.StrokeAnimal(null, _strokeAmount));
        }


        [Test]
        public void StrokeAnimal_With_Invalid_Happyness_Param_Throws_Exception()
        {
            var happynessAmount = -1;
            Assert.Throws<InvalidOperationException>(() => _test_AnimalManager.StrokeAnimal(_animal, happynessAmount));
        }

        [Test]
        public void StrokeAnimal_Returns_Same_Animal()
        {
            var result = _test_AnimalManager.StrokeAnimal(_animal, _strokeAmount);
            Assert.IsInstanceOf<Animal>(result);
            Assert.AreEqual(_animal.Id, result.Id);
            Assert.AreEqual(_animal.AnimalTypeId, result.AnimalTypeId);
        }

        [TestCase(1)]
        [TestCase(3)]
        [TestCase(5)]
        public void StrokeAnimal_Returns_Same_Animal_With_Updated_Happyness(int testValue)
        {
            var currentHappynessAmount = _animal.Happyness;
            var result = _test_AnimalManager.StrokeAnimal(_animal, testValue);
            Assert.AreEqual(currentHappynessAmount + testValue, result.Happyness);
        }

        [Test]
        public void StrokeAnimal_Returns_Same_Animal_With_Updated_Happyness_But_Unchanged_FoodSatisfaction()
        {
            var currentHappyness = _animal.Happyness;
            var result = _test_AnimalManager.StrokeAnimal(_animal, _strokeAmount);
            Assert.AreEqual(currentHappyness, result.FoodSatisfaction);
        }

        #endregion
    }
}
