﻿using AnimalProcessingAPI.Managers;
using AnimalProcessingAPI.Managers.Interfaces;
using AnimalProcessingAPI.Models;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Services.Models;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NUnit_AnimalProcessingAPI.Managers
{
    [ExcludeFromCodeCoverage]
    public class AnimalTypeManagerTests
    {
        private Mock<HttpWebRequest> _moq_HttpWebRequest;
        private Mock<IOptions<Config>> _moq_ConfigOptions;
        private Mock<IHttpWebRequestService> _moq_HttpWebRequestService;
        private AnimalTypeManager _test_AnimalTypeManager;

        private Guid _animalId = Guid.NewGuid();
        private Response<string> _responseString = new Response<string>();
        private AnimalType _animalType = new AnimalType();
        private Config _config = new Config();
        private HttpStatusCode _statusCode = HttpStatusCode.OK;

        [SetUp]
        public void Setup()
        {
            _moq_HttpWebRequest = new Mock<HttpWebRequest>();
            _moq_ConfigOptions = new Mock<IOptions<Config>>();
            _moq_ConfigOptions.Setup(moq => moq.Value).Returns(_config);
            _config.AnimalTypeAPIAddress = "TestAnimalTypeAPIAddress";

            _moq_HttpWebRequestService = new Mock<IHttpWebRequestService>();
            _moq_HttpWebRequestService.Setup(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>())).Returns(_moq_HttpWebRequest.Object);
            _moq_HttpWebRequestService.Setup(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>())).ReturnsAsync(_responseString);

            _responseString.StatusCode = _statusCode;
            _responseString.Data = JsonConvert.SerializeObject(_animalType);

            _test_AnimalTypeManager = new AnimalTypeManager(_moq_ConfigOptions.Object, _moq_HttpWebRequestService.Object);

        }

        #region Constructor Tests

        [Test]
        public void Constructor_Success()
        {
            Assert.DoesNotThrow(() => new AnimalManager(_moq_ConfigOptions.Object, _moq_HttpWebRequestService.Object));
        }

        [Test]
        public void Constructor_With_Invalid_HttpWebRequestService_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalManager(_moq_ConfigOptions.Object, null));
        }

        [Test]
        public void Constructor_With_Invalid_Config_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalManager(null, _moq_HttpWebRequestService.Object));
        }

        [Test]
        public void Constructor_Inherits_IAnimalManager()
        {
            Assert.IsInstanceOf<IAnimalTypeManager>(_test_AnimalTypeManager);
        }

        #endregion

        #region GetAnimalType Test

        [Test]
        public async Task GetAnimalType_Calls_httpWebRequestService_BuildWebRequest()
        {
            var result = await _test_AnimalTypeManager.GetAnimalType(_animalId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public async Task GetAnimalType_Calls_httpWebRequestService_BuildWebRequest_With_Expected_Address()
        {
            var result = await _test_AnimalTypeManager.GetAnimalType(_animalId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.Is<string>(v => v.Contains(_config.AnimalTypeAPIAddress)), It.IsAny<string>()));
        }
        [Test]
        public async Task GetAnimalType_Calls_httpWebRequestService_BuildWebRequest_With_Expected_Verb()
        {
            var expectedVerb = "GET";
            var result = await _test_AnimalTypeManager.GetAnimalType(_animalId);
            _moq_HttpWebRequestService.Verify(moq => moq.BuildWebRequest(It.IsAny<string>(), It.Is<string>(v => v == expectedVerb)));
        }

        [Test]
        public async Task GetAnimalType_Calls_httpWebRequestService_GetWebResponse()
        {
            var result = await _test_AnimalTypeManager.GetAnimalType(_animalId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.IsAny<HttpWebRequest>()));
        }

        [Test]
        public async Task GetAnimalType_Calls_httpWebRequestService_GetWebResponse_With_Expected_HttpWebRequest()
        {
            var result = await _test_AnimalTypeManager.GetAnimalType(_animalId);
            _moq_HttpWebRequestService.Verify(moq => moq.GetWebResponse(It.Is<HttpWebRequest>(v => v.Equals(_moq_HttpWebRequest.Object))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task GetAnimalType_Returns_NonOk_Status_Code_Returned_By_GetWebResponse(HttpStatusCode testCode)
        {

            _responseString.StatusCode = testCode;
            var result = await _test_AnimalTypeManager.GetAnimalType(_animalId);
            Assert.AreEqual(testCode, result.StatusCode);
        }

        [Test]
        public async Task GetAnimalType_Returns_Expected_Data()
        {
            var result = await _test_AnimalTypeManager.GetAnimalType(_animalId);
            Assert.AreEqual(_statusCode, result.StatusCode);
            Assert.AreEqual(_animalType.Id, result.Data.Id);
            Assert.AreEqual(_animalType.FoodSatisfactionGain, result.Data.FoodSatisfactionGain);
            Assert.AreEqual(_animalType.FoodSatisfactionLoss, result.Data.FoodSatisfactionLoss);
            Assert.AreEqual(_animalType.HappynessGain, result.Data.HappynessGain);
            Assert.AreEqual(_animalType.HappynessLoss, result.Data.HappynessLoss);
        }
        #endregion
    }
}
