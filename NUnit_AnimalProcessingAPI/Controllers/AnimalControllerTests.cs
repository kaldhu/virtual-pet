﻿using AnimalProcessingAPI.Controllers;
using AnimalProcessingAPI.Managers.Interfaces;
using AnimalProcessingAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using Services.Models;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NUnit_AnimalProcessingAPI.Controllers
{
    [ExcludeFromCodeCoverage]
    public class AnimalControllerTests
    {
        private Mock<IOptions<Config>> _moq_Config;
        private Mock<IAnimalManager> _moq_IAnimalManager;
        private Mock<IAnimalTypeManager> _moq_IAnimalTypeManager;
        private AnimalController _test_AnimalController;

        private Response<Animal> _animalResponse;
        private Response<AnimalType> _animalResponseType;
        private Animal _animal;
        private AnimalType _animalType;
        private Guid _animalId = Guid.NewGuid();
        private Guid _userId = Guid.NewGuid();
        private HttpStatusCode _statusCode = HttpStatusCode.OK;
        private int _intervalHours = 0;
        private int _intervalMinutes = 10;
        private int _intervalSeconds = 0;
        private int _lastUpdatedMinutes = -5;
        [SetUp]
        public void Setup()
        {
            _moq_Config = new Mock<IOptions<Config>>();
            _moq_Config.SetupGet(moq => moq.Value.LossIntervalHours).Returns(_intervalHours);
            _moq_Config.SetupGet(moq => moq.Value.LossIntervalMinutes).Returns(_intervalMinutes);
            _moq_Config.SetupGet(moq => moq.Value.LossIntervalSeconds).Returns(_intervalSeconds);

            _moq_IAnimalManager = new Mock<IAnimalManager>();
            _animal = new Animal()
            {
                LastUpdated = DateTime.Now.AddMinutes(_lastUpdatedMinutes)
            };
            _animalResponse = new Response<Animal>();
            _animalResponse.StatusCode = HttpStatusCode.OK;
            _animalResponse.Data = _animal;
            

            _moq_IAnimalManager.Setup(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(_animalResponse);
            _moq_IAnimalManager.Setup(moq => moq.CalculateStatLoss(It.IsAny<Animal>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_animal);
            _moq_IAnimalManager.Setup(moq => moq.UpdateAnimal(It.IsAny<Animal>(), It.IsAny<Guid>())).ReturnsAsync(_statusCode);
            _moq_IAnimalManager.Setup(moq => moq.FeedAnimal(It.IsAny<Animal>(), It.IsAny<int>())).Returns(_animal);
            _moq_IAnimalManager.Setup(moq => moq.StrokeAnimal(It.IsAny<Animal>(), It.IsAny<int>())).Returns(_animal);

            _moq_IAnimalTypeManager = new Mock<IAnimalTypeManager>();

            _animalType = new AnimalType();
            _animalResponseType = new Response<AnimalType>();
            _animalResponseType.StatusCode = HttpStatusCode.OK;
            _animalResponseType.Data = _animalType;

            _moq_IAnimalTypeManager.Setup(moq => moq.GetAnimalType(It.IsAny<Guid>())).ReturnsAsync(_animalResponseType);

            _test_AnimalController = new AnimalController(_moq_Config.Object, _moq_IAnimalManager.Object, _moq_IAnimalTypeManager.Object);
        }

        #region Constructor Tests

        [Test]
        public void Constructor_Success()
        {
            Assert.DoesNotThrow(() => new AnimalController(_moq_Config.Object, _moq_IAnimalManager.Object, _moq_IAnimalTypeManager.Object));
        }

        [Test]
        public void Constructor_With_Invalid_Config_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalController(null, _moq_IAnimalManager.Object, _moq_IAnimalTypeManager.Object));
        }
        [Test]
        public void Constructor_With_Invalid_AnimalManager_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalController(_moq_Config.Object, null, _moq_IAnimalTypeManager.Object));
        }
        [Test]
        public void Constructor_With_Invalid_AnimalTypeManager_Param_Throws_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new AnimalController(_moq_Config.Object, _moq_IAnimalManager.Object, null));
        }

        [Test]
        public void Constructor_Inherits_Controller()
        {
            Assert.IsInstanceOf<Controller>(_test_AnimalController);
        }

        #endregion

        #region Sync Tests

        [Test]
        public async Task Sync_Calls_AnimalManager_GetAnimal()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>()));
        }

        [Test]
        public async Task Sync_Calls_AnimalManager_GetAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.GetAnimal(It.Is<Guid>(v => v.Equals(_animalId)), It.Is<Guid>(v => v.Equals(_userId))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Sync_Returns_NonOk_StatusCode_When_AnimalManager_GetAnimal_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            var testResponse = new Response<Animal>()
            {
                Data = null,
                StatusCode = test_Value
            };
            _moq_IAnimalManager.Setup(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(testResponse);
            var result = await _test_AnimalController.Sync(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Sync_Calls_AnimalTypeManager_GetAnimalType()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalTypeManager.Verify(moq => moq.GetAnimalType(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Sync_Calls_AnimalTypeManager_GetAnimalType_With_Expected_Values()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalTypeManager.Verify(moq => moq.GetAnimalType(It.Is<Guid>(v => v.Equals(_animal.AnimalTypeId))));
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Sync_Returns_NonOk_StatusCode_When_AnimalTypeManager_GetAnimalType_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            var testResponse = new Response<AnimalType>()
            {
                Data = null,
                StatusCode = test_Value
            };
            _moq_IAnimalTypeManager.Setup(moq => moq.GetAnimalType(It.IsAny<Guid>())).ReturnsAsync(testResponse);
            var result = await _test_AnimalController.Sync(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Sync_Calls_AnimalManager_CalculateStatLoss()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.CalculateStatLoss(It.IsAny<Animal>(), It.IsAny<int>(), It.IsAny<int>()));
        }

        [Test]
        public async Task Sync_Calls_AnimalManager_CalculateStatLoss_With_Expected_Values()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => 
                moq.CalculateStatLoss(
                    It.Is<Animal>(v => v.Equals(_animal)), 
                    It.Is<int>(v => v == _animalType.HappynessLoss),
                    It.Is<int>(v => v == _animalType.FoodSatisfactionLoss)));
        }


        [Test]
        public async Task Sync_Calls_AnimalManager_UpdateAnimal()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.UpdateAnimal(It.IsAny<Animal>(), It.IsAny<Guid>()));
        }

        [Test]
        public async Task Sync_Calls_AnimalManager_UpdateAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Sync(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => 
                moq.UpdateAnimal(
                    It.Is<Animal>(v => v.Equals(_animal)),
                    It.Is<Guid>(v => v.Equals(_userId))));
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Sync_Returns_NonOk_StatusCode_When_AnimalManager_UpdateAnimal_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            _moq_IAnimalManager.Setup(moq => moq.UpdateAnimal(It.IsAny<Animal>(), It.IsAny<Guid>())).ReturnsAsync(test_Value);
            var result = await _test_AnimalController.Sync(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        #endregion

        #region Feed Tests

        [Test]
        public async Task Feed_Calls_AnimalManager_GetAnimal()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>()));
        }

        [Test]
        public async Task Feed_Calls_AnimalManager_GetAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.GetAnimal(It.Is<Guid>(v => v.Equals(_animalId)), It.Is<Guid>(v => v.Equals(_userId))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Feed_Returns_NonOk_StatusCode_When_AnimalManager_GetAnimal_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            var testResponse = new Response<Animal>()
            {
                Data = null,
                StatusCode = test_Value
            };
            _moq_IAnimalManager.Setup(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(testResponse);
            var result = await _test_AnimalController.Feed(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Feed_Returns_UnprocessableEntity_Status_If_Animal_Not_Been_Synced()
        {
            _animal = new Animal()
            {
                LastUpdated = DateTime.Now.AddDays(-1)
            };
            _animalResponse = new Response<Animal>();
            _animalResponse.StatusCode = HttpStatusCode.OK;
            _animalResponse.Data = _animal;


            _moq_IAnimalManager.Setup(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(_animalResponse);


            var result = await _test_AnimalController.Feed(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.UnprocessableEntity, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Feed_Calls_AnimalTypeManager_GetAnimalType()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalTypeManager.Verify(moq => moq.GetAnimalType(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Feed_Calls_AnimalTypeManager_GetAnimalType_With_Expected_Values()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalTypeManager.Verify(moq => moq.GetAnimalType(It.Is<Guid>(v => v.Equals(_animal.AnimalTypeId))));
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Feed_Returns_NonOk_StatusCode_When_AnimalTypeManager_GetAnimalType_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            var testResponse = new Response<AnimalType>()
            {
                Data = null,
                StatusCode = test_Value
            };
            _moq_IAnimalTypeManager.Setup(moq => moq.GetAnimalType(It.IsAny<Guid>())).ReturnsAsync(testResponse);
            var result = await _test_AnimalController.Feed(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Feed_Calls_AnimalManager_FeedAnimal()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.FeedAnimal(It.IsAny<Animal>(), It.IsAny<int>()));
        }

        [Test]
        public async Task Feed_Calls_AnimalManager_FeedAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq =>
                moq.FeedAnimal(
                    It.Is<Animal>(v => v.Equals(_animal)),
                    It.Is<int>(v => v == _animalType.FoodSatisfactionGain)));
        }


        [Test]
        public async Task Feed_Calls_AnimalManager_UpdateAnimal()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.UpdateAnimal(It.IsAny<Animal>(), It.IsAny<Guid>()));
        }

        [Test]
        public async Task Feed_Calls_AnimalManager_UpdateAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Feed(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq =>
                moq.UpdateAnimal(
                    It.Is<Animal>(v => v.Equals(_animal)),
                    It.Is<Guid>(v => v.Equals(_userId))));
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Feed_Returns_NonOk_StatusCode_When_AnimalManager_UpdateAnimal_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            _moq_IAnimalManager.Setup(moq => moq.UpdateAnimal(It.IsAny<Animal>(), It.IsAny<Guid>())).ReturnsAsync(test_Value);
            var result = await _test_AnimalController.Feed(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        #endregion

        #region Stroke Tests

        [Test]
        public async Task Stroke_Calls_AnimalManager_GetAnimal()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>()));
        }

        [Test]
        public async Task Stroke_Calls_AnimalManager_GetAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.GetAnimal(It.Is<Guid>(v => v.Equals(_animalId)), It.Is<Guid>(v => v.Equals(_userId))));
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Stroke_Returns_NonOk_StatusCode_When_AnimalManager_GetAnimal_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            var testResponse = new Response<Animal>()
            {
                Data = null,
                StatusCode = test_Value
            };
            _moq_IAnimalManager.Setup(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(testResponse);
            var result = await _test_AnimalController.Stroke(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Stroke_Returns_UnprocessableEntity_Status_If_Animal_Not_Been_Synced()
        {
            _animal = new Animal()
            {
                LastUpdated = DateTime.Now.AddDays(-1)
            };
            _animalResponse = new Response<Animal>();
            _animalResponse.StatusCode = HttpStatusCode.OK;
            _animalResponse.Data = _animal;


            _moq_IAnimalManager.Setup(moq => moq.GetAnimal(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(_animalResponse);


            var result = await _test_AnimalController.Stroke(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)HttpStatusCode.UnprocessableEntity, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Stroke_Calls_AnimalTypeManager_GetAnimalType()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalTypeManager.Verify(moq => moq.GetAnimalType(It.IsAny<Guid>()));
        }

        [Test]
        public async Task Stroke_Calls_AnimalTypeManager_GetAnimalType_With_Expected_Values()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalTypeManager.Verify(moq => moq.GetAnimalType(It.Is<Guid>(v => v.Equals(_animal.AnimalTypeId))));
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Stroke_Returns_NonOk_StatusCode_When_AnimalTypeManager_GetAnimalType_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            var testResponse = new Response<AnimalType>()
            {
                Data = null,
                StatusCode = test_Value
            };
            _moq_IAnimalTypeManager.Setup(moq => moq.GetAnimalType(It.IsAny<Guid>())).ReturnsAsync(testResponse);
            var result = await _test_AnimalController.Stroke(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        [Test]
        public async Task Stroke_Calls_AnimalManager_StrokeAnimal()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.StrokeAnimal(It.IsAny<Animal>(), It.IsAny<int>()));
        }

        [Test]
        public async Task Stroke_Calls_AnimalManager_StrokeAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq =>
                moq.StrokeAnimal(
                    It.Is<Animal>(v => v.Equals(_animal)),
                    It.Is<int>(v => v == _animalType.HappynessGain)));
        }


        [Test]
        public async Task Stroke_Calls_AnimalManager_UpdateAnimal()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq => moq.UpdateAnimal(It.IsAny<Animal>(), It.IsAny<Guid>()));
        }

        [Test]
        public async Task Stroke_Calls_AnimalManager_UpdateAnimal_With_Expected_Values()
        {
            await _test_AnimalController.Stroke(_animalId, _userId);
            _moq_IAnimalManager.Verify(moq =>
                moq.UpdateAnimal(
                    It.Is<Animal>(v => v.Equals(_animal)),
                    It.Is<Guid>(v => v.Equals(_userId))));
        }


        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.Unauthorized)]
        [TestCase(HttpStatusCode.Conflict)]
        [TestCase(HttpStatusCode.InternalServerError)]
        public async Task Stroke_Returns_NonOk_StatusCode_When_AnimalManager_UpdateAnimal_Returns_NonOk_StatusCode(HttpStatusCode test_Value)
        {
            _moq_IAnimalManager.Setup(moq => moq.UpdateAnimal(It.IsAny<Animal>(), It.IsAny<Guid>())).ReturnsAsync(test_Value);
            var result = await _test_AnimalController.Stroke(_animalId, _userId);
            Assert.IsInstanceOf<StatusCodeResult>(result);
            var statusCodeResult = (StatusCodeResult)result;
            Assert.AreEqual((int)test_Value, statusCodeResult.StatusCode);
        }

        #endregion
    }
}
