﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AnimalProcessingAPI.Managers.Interfaces;
using AnimalProcessingAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Services.Services;

namespace AnimalProcessingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalController : Controller
    {
        private readonly IAnimalManager _animalManager;
        private readonly IAnimalTypeManager _animalTypeManager;
        private readonly IOptions<Config> _config;

        public AnimalController(IOptions<Config> config, IAnimalManager animalManager, IAnimalTypeManager animalTypeManager)
        {
            if(animalManager == null || animalTypeManager == null || config == null)
            {
                throw new ArgumentNullException();
            }
            _animalTypeManager = animalTypeManager;
            _animalManager = animalManager;
            _config = config;
        }

        [HttpPut ("{id}/sync")]
        public async Task<ActionResult> Sync(Guid id, Guid userId)
        {
            #region Get Animal Data

            var animalResponse = await _animalManager.GetAnimal(id, userId);
            if(animalResponse.StatusCode != HttpStatusCode.OK)
            {
                return StatusCode((int)animalResponse.StatusCode);
            }

            #endregion

            #region Get AnimalType Data

            var animalTypeResponse = await _animalTypeManager.GetAnimalType(animalResponse.Data.AnimalTypeId);
            if (animalTypeResponse.StatusCode != HttpStatusCode.OK)
            {
                return StatusCode((int)animalTypeResponse.StatusCode);
            }

            #endregion

            #region Update FoodSatisfaction and Happyness

            var animal = _animalManager.CalculateStatLoss(
                animalResponse.Data,
                animalTypeResponse.Data.HappynessLoss,
                animalTypeResponse.Data.FoodSatisfactionLoss);

            var updateResult = await _animalManager.UpdateAnimal(animal, userId);

            if(updateResult != HttpStatusCode.OK)
            {
                return StatusCode((int)updateResult);
            }

            #endregion

            return Ok();
        }

        [HttpPut("{id}/feed")]
        public async Task<ActionResult> Feed(Guid id, Guid userId)
        {

            var animalResponse = await _animalManager.GetAnimal(id, userId);
            if (animalResponse.StatusCode != HttpStatusCode.OK)
            {
                return StatusCode((int)animalResponse.StatusCode);
            }

            var timeSinceLastUpdated = DateTime.Now.Subtract(animalResponse.Data.LastUpdated);
            
            if (timeSinceLastUpdated > new TimeSpan(_config.Value.LossIntervalHours, _config.Value.LossIntervalMinutes, _config.Value.LossIntervalSeconds))
            {
                return StatusCode((int)HttpStatusCode.UnprocessableEntity);
            }

            var animalTypeResponse = await _animalTypeManager.GetAnimalType(animalResponse.Data.AnimalTypeId);
            if (animalTypeResponse.StatusCode != HttpStatusCode.OK)
            {
                return StatusCode((int)animalTypeResponse.StatusCode);
            }

            var animal = _animalManager.FeedAnimal(animalResponse.Data, animalTypeResponse.Data.FoodSatisfactionGain);

            var updateResult = await _animalManager.UpdateAnimal(animal, userId);
            if (updateResult != HttpStatusCode.OK)
            {
                return StatusCode((int)updateResult);
            }

            return Json(animal);
        }

        [HttpPut("{id}/stroke")]
        public async Task<ActionResult> Stroke(Guid id, Guid userId)
        {
            #region Get Animal Data

            var animalResponse = await _animalManager.GetAnimal(id, userId);
            if (animalResponse.StatusCode != HttpStatusCode.OK)
            {
                return StatusCode((int)animalResponse.StatusCode);
            }

            var timeSinceLastUpdated = DateTime.Now.Subtract(animalResponse.Data.LastUpdated);
            if (timeSinceLastUpdated > new TimeSpan(_config.Value.LossIntervalHours, _config.Value.LossIntervalMinutes, _config.Value.LossIntervalSeconds))
            {
                return StatusCode((int)HttpStatusCode.UnprocessableEntity);
            }
            #endregion

            #region Get AnimalType Data

            var animalTypeResponse = await _animalTypeManager.GetAnimalType(animalResponse.Data.AnimalTypeId);
            if (animalTypeResponse.StatusCode != HttpStatusCode.OK)
            {
                return StatusCode((int)animalTypeResponse.StatusCode);
            }

            #endregion

            var animal = _animalManager.StrokeAnimal(animalResponse.Data, animalTypeResponse.Data.FoodSatisfactionGain);

            var updateResult = await _animalManager.UpdateAnimal(animal, userId);
            if (updateResult != HttpStatusCode.OK)
            {
                return StatusCode((int)updateResult);
            }

            return Json(animal);
        }
    }
}