﻿using AnimalProcessingAPI.Managers.Interfaces;
using AnimalProcessingAPI.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Services.Models;
using Services.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AnimalProcessingAPI.Managers
{
    internal class AnimalTypeManager : IAnimalTypeManager
    {
        private readonly IOptions<Config> _config;
        private readonly IHttpWebRequestService _httpWebRequestService;

        public AnimalTypeManager(IOptions<Config> config, IHttpWebRequestService httpWebRequestService)
        {
            if (config == null || httpWebRequestService == null)
            {
                throw new ArgumentNullException();
            }
            _config = config;
            _httpWebRequestService = httpWebRequestService;
        }

        public async Task<Response<AnimalType>> GetAnimalType(Guid id)
        {
            var response = new Response<AnimalType>();

            var animalRequestString = _config.Value.AnimalTypeAPIAddress + $"/{id}";
            var animalWebRequest = _httpWebRequestService.BuildWebRequest(animalRequestString, HttpMethod.Get.Method);
            var animalWebRequestResult = await _httpWebRequestService.GetWebResponse(animalWebRequest);

            response.StatusCode = animalWebRequestResult.StatusCode;

            if (animalWebRequestResult.StatusCode != HttpStatusCode.OK)
            {
                return response;
            }

            response.Data = JsonConvert.DeserializeObject<AnimalType>(animalWebRequestResult.Data);

            return response;
        }
    }
}
