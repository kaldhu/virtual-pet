﻿using AnimalProcessingAPI.Managers.Interfaces;
using AnimalProcessingAPI.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Services.Models;
using Services.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AnimalProcessingAPI.Managers
{
    internal class AnimalManager : IAnimalManager
    {
        private readonly int _maxHappyness = 100;
        private readonly int _minHappyness = -100;
        private readonly int _maxFoodSatisfaction = 100;
        private readonly int _minFoodSatisfaction = -100;
        private readonly TimeSpan _lossInterval;

        private readonly IOptions<Config> _config;
        private readonly IHttpWebRequestService _httpWebRequestService;

        public AnimalManager(IOptions<Config> config, IHttpWebRequestService httpWebRequestService)
        {
            if (config == null || httpWebRequestService == null)
            {
                throw new ArgumentNullException();
            }
            _config = config;
            _maxHappyness = _config.Value.MaxHappyness;
            _minHappyness = _config.Value.MinHappyness;
            _maxFoodSatisfaction = _config.Value.MaxFoodSatisfaction;
            _minFoodSatisfaction = _config.Value.MinFoodSatisfaction;

            _lossInterval = new TimeSpan(
                _config.Value.LossIntervalHours,
                _config.Value.LossIntervalMinutes,
                _config.Value.LossIntervalSeconds);

            _httpWebRequestService = httpWebRequestService;
        }


        public async Task<Response<Animal>> GetAnimal(Guid id, Guid userId)
        {
            var response = new Response<Animal>();
            
            var animalRequestString = _config.Value.AnimalAPIAddress + $"/{id}?userId={userId}";
            var animalWebRequest = _httpWebRequestService.BuildWebRequest(animalRequestString, HttpMethod.Get.Method);
            Response<string> animalWebRequestResult;
            try
            {
                animalWebRequestResult = await _httpWebRequestService.GetWebResponse(animalWebRequest);
            } 
            catch (Exception ex)
            {
                return new Response<Animal>()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }
            response.StatusCode = animalWebRequestResult.StatusCode;

            if (animalWebRequestResult.StatusCode != HttpStatusCode.OK)
            {
                return response;
            }

            response.Data = JsonConvert.DeserializeObject<Animal>(animalWebRequestResult.Data);
            return response;
        }

        public async Task<HttpStatusCode> UpdateAnimal(Animal animal, Guid userId)
        {
            var animalRequestString = _config.Value.AnimalAPIAddress + $"/{animal.Id}?userId={userId}";
            var animalWebRequest = _httpWebRequestService.BuildWebRequest(animalRequestString, HttpMethod.Put.Method);
            await _httpWebRequestService.AddDataToRequest(animalWebRequest, animal);

            try
            {
                var animalWebRequestResult = await _httpWebRequestService.GetWebResponse(animalWebRequest);
                return animalWebRequestResult.StatusCode;

            } 
            catch (Exception ex)
            {
                return HttpStatusCode.InternalServerError;
            }

        }

        public Animal CalculateStatLoss(Animal animal, int happynessLoss, int foodSatisfactionLoss)
        {
            if(animal == null)
            {
                throw new ArgumentNullException();
            }

            if(happynessLoss < 0 || foodSatisfactionLoss < 0 )
            {
                throw new InvalidOperationException();
            }

            var timeTranspired = DateTime.UtcNow.Subtract(animal.LastUpdated);

            if(timeTranspired.CompareTo(_lossInterval) <= 0)
            {
                return animal;
            }

            var numberOfIntervals = timeTranspired.Ticks / _lossInterval.Ticks;

            var totalHappynessLoss = numberOfIntervals * happynessLoss;
            var totalFoodSatisfactionLoss = numberOfIntervals * foodSatisfactionLoss;

            var newHappynessValue = animal.Happyness - (int)totalHappynessLoss;
            var newFoodSatisfactionValue = animal.FoodSatisfaction - (int)totalFoodSatisfactionLoss;

            animal.Happyness = newHappynessValue < _minHappyness ? _minHappyness : newHappynessValue;
            animal.FoodSatisfaction = newFoodSatisfactionValue < _minFoodSatisfaction ? _minFoodSatisfaction : newFoodSatisfactionValue;

            return animal;
        }

        public Animal FeedAnimal(Animal animal, int feedAmount)
        {
            if (animal == null)
            {
                throw new ArgumentNullException();
            }
            if (feedAmount < 0)
            {
                throw new InvalidOperationException();
            }
            var newFoodSatisfactionValue = animal.FoodSatisfaction + feedAmount;
            animal.FoodSatisfaction = newFoodSatisfactionValue > _maxFoodSatisfaction ? _maxFoodSatisfaction : newFoodSatisfactionValue;
            return animal;
        }

        public Animal StrokeAnimal(Animal animal, int happynessAmount)
        {
            if (animal == null)
            {
                throw new ArgumentNullException();
            }
            if (happynessAmount < 0)
            {
                throw new InvalidOperationException();
            }
            var newHappynessValue = animal.Happyness + happynessAmount;
            animal.Happyness = newHappynessValue > _maxHappyness ? _maxHappyness : newHappynessValue;
            return animal;
        }

    }
}
