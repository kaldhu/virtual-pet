﻿using AnimalProcessingAPI.Models;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnimalProcessingAPI.Managers.Interfaces
{
    public interface IAnimalTypeManager
    {
        Task<Response<AnimalType>> GetAnimalType(Guid id);
    }
}
