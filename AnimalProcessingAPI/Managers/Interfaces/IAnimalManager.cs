﻿using AnimalProcessingAPI.Models;
using Services.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AnimalProcessingAPI.Managers.Interfaces
{
    public interface IAnimalManager
    {
        Task<Response<Animal>> GetAnimal(Guid id, Guid userId);
        Task<HttpStatusCode> UpdateAnimal(Animal animal, Guid userId);
        Animal CalculateStatLoss(Animal animal, int happynessLoss, int foodSatisfactionLoss);
        Animal FeedAnimal(Animal animal, int feedAmount);
        public Animal StrokeAnimal(Animal animal, int happynessAmount);


    }
}
