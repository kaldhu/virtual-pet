﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnimalProcessingAPI.Models
{
    public class AnimalType
    {
        public virtual Guid Id { get; set; }
        public int HappynessGain { get; set; }
        public int HappynessLoss { get; set; } 
        public int FoodSatisfactionGain { get; set; }
        public int FoodSatisfactionLoss { get; set; }
    }
}
