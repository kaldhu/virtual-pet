﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnimalProcessingAPI.Models
{
    public class Animal
    {
        public virtual Guid Id { get; set; }
        public virtual Guid UserId { get; set; }
        public virtual Guid AnimalTypeId { get; set; }
        public int Happyness { get; set; }
        public int FoodSatisfaction { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}
