﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnimalProcessingAPI.Models
{
    public class Config
    {
        public string AnimalAPIAddress { get; set; }
        public string AnimalTypeAPIAddress { get; set; }
        public virtual int MinHappyness { get; set; }
        public virtual int MaxHappyness { get; set; }
        public virtual int MinFoodSatisfaction { get; set; }
        public virtual int MaxFoodSatisfaction { get; set; }
        public virtual int LossIntervalHours { get; set; }
        public virtual int LossIntervalMinutes { get; set; }
        public virtual int LossIntervalSeconds { get; set; }
    }
}
